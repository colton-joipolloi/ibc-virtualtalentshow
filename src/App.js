import React, { useEffect } from 'react';
import { Authenticator } from 'aws-amplify-react';
import Amplify from 'aws-amplify';
import awsconfig from './aws-exports';

import AuthWrapper from './layout/AuthWrapper/AuthWrapper';

import './App.scss';

Amplify.configure(awsconfig);

function App() {
    useEffect(() => {
        function setVh() {
            const vh = window.innerHeight * 0.01;
            document.documentElement.style.setProperty('--vh', `${vh}px`);
        }
        setVh();
        window.addEventListener('resize', setVh);
        return function cleanup() {
            window.removeEventListener('resize', setVh);
        };
    }, []);

    return (
        <div className='App'>
            <Authenticator hideDefault={true} amplifyConfig={awsconfig}>
                <AuthWrapper />
            </Authenticator>
        </div>
    );
}

export default App;
