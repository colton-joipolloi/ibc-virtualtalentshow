import React from 'react';
import ReactHowler from 'react-howler'

import {
    withRouter
} from 'react-router-dom'

class SelectAudio extends React.Component {
    constructor (props) {
      super(props)
  
      this.state = {
        playing: false,
        selectedCategory: null
      }
      this.handlePlay = this.handlePlay.bind(this)
      this.handlePause = this.handlePause.bind(this)
    }
  
    handlePlay() {
      this.setState({
        playing: true
      })
    }
  
    handlePause() {
      this.setState({
        playing: false
      })
    }

    setCategory(cat){
        this.setState({selectedCategory: cat})
    }
  
    render () {
      let selector;
      if (!this.state.selectedCategory){
        if (this.props.categories){
          selector = (
            <>
              {
                this.props.categories.map((value, index) => {
                  console.log(value, index);
                  return (
                    <div class="row-around">
                      <div class="col">
                        <button className="red icon-text big" onClick={() => this.setCategory(value.dir)}>
                          { value.dir.charAt(0).toUpperCase() + value.dir.slice(1) } 
                        </button>
                      </div>
                    </div>
                  )
                })
              }
            </>
          );
        } else {
          selector = (
            <p>Loading categories...</p>
          );
        }
      } else {
        if (!this.props.tracks){
          selector = (
            <p>Loading tracks...</p>
          );
        }
        else {
          selector = (
            <div class="list">
              {
                this.props.tracks.map((value, index) => {
                  // console.log(value, index);
                  if (value.category === this.state.selectedCategory){
                    return (
                      <div className="item" style={{backgroundColor: this.props.selectedTrack === value.url? "grey-500" : null}}>
                          <h2>{value.track}</h2>
                          <p> By: artist</p>
                          <p> Genre: {value.category}</p>
                          <div className="right">
                          <button className="blue radius" onClick={() => this.props.select({ url : value.url, name : value.track, genre : value.genre })}>Select</button>
                          </div>
                      </div>
                    )
                  }
                  return null
                })
              }
            </div>
          );
        }
      }
  
      return (
        <>
            <div className="row-around" style={{ padding: "5vh", marginTop: "5vh", textAlign: "center" }}>
                <div className="col">
                    <ReactHowler
                        playing={this.state.playing}
                        src={this.props.selectedTrack.url}
                    />
                
                <div className="padding">
                    <button className="green circle icon ion-ios-play" onClick={this.handlePlay}></button>
                    <button className="red circle icon ion-stop shadow" onClick={this.handlePause}></button>
                </div>
                </div>
            </div>

            {this.props.selectedTrack.name}
  
            {selector}

            <div style={{ padding: "5vh", marginTop: "5vh", textAlign: "center" }} >
                <button className="icon-text blue-grey-900" onClick={() => this.props.history.push("/record") }>
                    <i class="icon ion-videocamera"></i>
                    Record
                </button>
            </div>
        </>
      )
    }
}

export default withRouter(SelectAudio)