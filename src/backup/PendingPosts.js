
import React from 'react'
import { withAuthenticator } from "@aws-amplify/ui-react"
import { Storage } from 'aws-amplify'
/* import VideoPlayer from './VideoPlayer' */

class PendingPosts extends React.Component {
    /* 
    constructor (props) {
        super(props)  

        Storage.list('uploads', { level: 'protected' })
        .then(result => console.log(result))
        .catch(err => console.log(err));

        Storage.list('uploads', { level: 'private' })
        .then(result => console.log(result))
        .catch(err => console.log(err));
    }
    */

    state = { 
        uriArray: []
    };

    componentDidMount(){
        this.fetchPosts();
    }

    async fetchPosts(){
        await Storage.list('uploads', { level: 'protected' })
        .then( async result => {
            console.log(result);

            let list = async () => {

                let arr = [];
                
                result.forEach(async e => {
                    await Storage.get(e.key, { level: 'protected' })
                    .then(post => {
                        console.log(post);
                        arr[arr.length] = {url : post};
                    });
                })

                return arr;
            };

            await list().then((value) => this.setState({uriArray: value}))
        })
        .catch(err => console.log(err));
    }

    render() {

        if (this.state.uriArray){

            console.log(this.state.uriArray.length);
            
            return (
                <div>
                  {this.state.uriArray.map(post => (
                    <a href={post.url}>Download</a>
                  ))}
                </div>
            );/* 
            .forEach(post => {
                console.log(post);
                const videoJsOptions = {
                    autoplay: false,
                    controls: true,
                    sources: [{
                      src: post.url,//window.URL.createObjectURL(uri),
                      type: 'video/webm'
                    }]
                };
                return (
                    <div>
                        <VideoPlayer { ...videoJsOptions } />
                        <i href={post.url}>Download</i>
                    </div>
                )
            }); */
        }
        else{
            return (
                <div>Loading...</div>
            );
        }
    }
}

export default withAuthenticator(PendingPosts, false);