import React from 'react';

import KareokeRecorder from './components/KareokeRecorder/KareokeRecorder';
import KareokeRecorderActions from './components/KareokeRecorder/Actions';

import { withRouter } from 'react-router-dom';
import { Storage } from 'aws-amplify';

import API, { graphqlOperation } from '@aws-amplify/api';
// import * as queries from './graphql/queries'
import * as mutations from './graphql/mutations';
// import * as subscriptions from './graphql/subscriptions'

class Record extends React.Component {
    constructor(props) {
        super(props);

        var { selectedTrack } = this.props;
        console.log(selectedTrack);

        this.state = {
            playBackingTrack: false,
            selectedAudio: selectedTrack,
            status: 'recording_perms',
        };

        this.backingTrack = this.backingTrack.bind(this);
        this.uploadFile = this.uploadFile.bind(this);

        var MIME_TYPES = [
            'video/webm;codecs="vp8,opus"',
            'video/webm;codecs=h264',
            'video/webm;codecs=vp9',
            'video/webm',
        ];

        // AWS Rekognition supports MP4 & MOV - Let's attempt to upload in either of those two before defaulting to whatever we can support...
        var options;

        if (MediaRecorder.isTypeSupported('video/mp4;codecs=h264')) {
            options = { mimeType: 'video/mp4; codecs=h264' };
        } else if (MediaRecorder.isTypeSupported('video/mov;codecs=h264')) {
            options = { mimeType: 'video/mov; codecs=h264' };
        } else {
            options = {
                mimeType: window.MediaRecorder.isTypeSupported
                    ? MIME_TYPES.find(window.MediaRecorder.isTypeSupported)
                    : 'video/webm',
            };
        }

        this.options = options;
    }

    backingTrack(bool) {
        this.setState({
            playBackingTrack: bool,
        });
    }

    uploadFile() {
        const file = this.props.recording;
        var ext = this.options.mimeType.split('/');
        ext = ext[1].split(';');
        ext = '.' + ext[0];

        const name = Math.random().toString(36).slice(2) + ext;

        Storage.put(name, file, {
            contentType: this.options.mimeType,
            level: 'public',
            progressCallback(progress) {
                console.log(`Uploaded: ${progress.loaded}/${progress.total}`);
            },
        })
            .then((result) => {
                console.log(result);

                var videoS3Key = result.key;

                // pesist a record with the s3 key id & the user that submitted it (user is automatically stored via the Amplify magic)
                API.graphql(
                    graphqlOperation(mutations.createPost, {
                        input: {
                            videoS3Key,
                        },
                    })
                );

                this.props.history.push('/submitted');
            })
            .catch((err) => console.log(err));
    }

    render() {
        let notify;

        switch (this.state.status) {
            case 'start':
                notify = (
                    <>
                        <p>
                            When you're ready, press the red RECORD button and
                            the timer will count you down.
                        </p>
                        <p>Remember, HEADPHONES on!</p>
                    </>
                );
                break;
            case 'get_ready':
                notify = <h1>Get ready to sing!</h1>;
                break;
            case 'recording':
                notify = <h1 className='red-900'>Recording</h1>;
                break;
            case 'complete':
                notify = <h1>Recording complete</h1>;
                break;
            default:
                notify = '';
                break;
        }

        /*   
        onStartRecording: PropTypes.func,
        onStopRecording: PropTypes.func,
        onPauseRecording: PropTypes.func,
        onResumeRecording: PropTypes.func,
        onRecordingComplete: PropTypes.func,
        onOpenVideoInput: PropTypes.func,
        onStopReplaying: PropTypes.func,
        onError: PropTypes.func 
      */

        return (
            <div>
                <div
                    style={{
                        paddingTop: '5vh',
                        paddingRight: '5vw',
                        paddingLeft: '5vw',
                        marginTop: '5vh',
                        textAlign: 'center',
                    }}
                >
                    {notify}
                </div>
                <div style={{ height: '50vh' }}>
                    <KareokeRecorder
                        mediaRecorderOpts={this.options}
                        isOnInitially={true}
                        isFlipped={true}
                        showReplayControls={true}
                        replayVideoAutoplayAndLoopOff={true}
                        renderActions={KareokeRecorderActions}
                        selectedAudio={this.state.selectedAudio}
                        style={{ height: '60vh' }}
                        dataAvailableTimeout={10000}
                        onCameraOn={() => {
                            this.setState({ status: 'start' });
                        }}
                        onStartRecording={() => {
                            this.setState({ status: 'recording' });
                        }}
                        onStopReplaying={() => {
                            this.setState({ status: null });
                        }}
                        onRecordingComplete={(videoBlob) => {
                            this.props.record(videoBlob);
                            this.setState({ status: 'complete' });
                        }}
                    />
                </div>
                <div
                    style={{
                        padding: '5vh',
                        marginTop: '5vh',
                        textAlign: 'center',
                    }}
                >
                    {this.state.status === 'complete' ? (
                        <>
                            <button
                                className='icon-text blue-grey-900'
                                onClick={this.uploadFile}
                            >
                                <i class='icon ion-paper-airplane'></i>
                                Submit
                            </button>
                        </>
                    ) : null}
                </div>
            </div>
        );
    }
}

export default withRouter(Record);
