import React from 'react';
import { Authenticator } from 'aws-amplify-react';
import Amplify from 'aws-amplify';
import awsconfig from './aws-exports';

import AuthWrapper from './layout/AuthWrapper/AuthWrapper';

Amplify.configure(awsconfig);

class App extends React.Component {
    constructor(props) {
        super(props);
        this.record = this.record.bind(this);
    }

    state = {
        username: '',
        latestVideoBlob: null,
        nextEnabled: false,
        selectedTrack: {
            url: '/tracks/30_seconds_of_tom.m4a',
            name: 'Annoying Tom Jones track',
            genre: 'Irritating',
        },
    };

    async componentDidMount() {}

    record(videoBlob) {
        console.log(videoBlob);
        this.setState({ latestVideoBlob: videoBlob });
    }

    render() {
        return (
            <div className='App'>
                <Authenticator hideDefault={true} amplifyConfig={awsconfig}>
                    <AuthWrapper />
                </Authenticator>
            </div>
            // <Router>
            //     <div className='grey-100'>
            //         <div className='header indigo'>
            //             <div className='left'>
            //                 {window.location.pathname !== '/' ? (
            //                     <HistoryBack />
            //                 ) : (
            //                     ''
            //                 )}
            //             </div>
            //             <div className='title'></div>
            //         </div>
            //         <div className='header sub indigo-400'>
            //             <div className='buttons-group full'>
            //                 <button
            //                     className='circle icon ion-ios-home radius-left'
            //                     onClick={() => (window.location.href = '/')}
            //                 ></button>
            //                 <button
            //                     className='circle icon ion-ios-musical-notes'
            //                     onClick={() =>
            //                         (window.location.href = '/select')
            //                     }
            //                 ></button>
            //                 <button
            //                     className='circle icon ion-android-contact radius-right'
            //                     onClick={() => window.openMenu('myMenu')}
            //                 ></button>
            //             </div>
            //         </div>

            //         <div className='menu white menu-right' id='myMenu'>
            //             <div className='list no-border'>
            //                 <div className='item blue'>
            //                     <div className='left'>
            //                         <img
            //                             className='avatar circle'
            //                             src='/img/80.jpg'
            //                             alt='avatar'
            //                         />
            //                     </div>
            //                     <p> Signed in as: {this.state.username} </p>
            //                 </div>
            //                 <a className='item' href='/profile'>
            //                     <div className='left'>
            //                         <i className='icon ion-ios-list-outline'></i>
            //                     </div>
            //                     <h1>My profile</h1>
            //                 </a>
            //                 <a className='item'>
            //                     <AmplifySignOut />
            //                 </a>
            //             </div>
            //         </div>

            //         <div className='content has-header padding'>
            //             <div
            //                 className='container'
            //                 style={{
            //                     padding: '5vh',
            //                     marginTop: '5vh',
            //                     textAlign: 'center',
            //                 }}
            //             >
            //                 <Switch>
            //                     <Route exact path='/'>
            //                         <Feed bucket={this.approvedBucket} />
            //                     </Route>
            //                     <Route exact path='/record'>
            //                         <Record
            //                             record={this.record}
            //                             selectedTrack={this.state.selectedTrack}
            //                             recording={this.state.latestVideoBlob}
            //                         />
            //                     </Route>
            //                     <Route exact path='/submitted'>
            //                         <VideoSubmitted />
            //                     </Route>
            //                     <Route exact path='/select'>
            //                         <SelectAudio
            //                             select={this.setAudioTrack}
            //                             categories={this.state.categories}
            //                             selectedTrack={this.state.selectedTrack}
            //                             tracks={this.state.tracks}
            //                         />
            //                     </Route>
            //                     <Route exact path='/post/pending'>
            //                         <PendingPosts />
            //                     </Route>
            //                 </Switch>
            //             </div>
            //         </div>
            //     </div>
            // </Router>
        );
    }
}

export default App;
