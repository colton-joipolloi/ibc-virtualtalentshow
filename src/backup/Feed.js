
import React from 'react'
import { Storage } from 'aws-amplify'

class Feed extends React.Component {

    constructor (props) {
        super(props)

        var { bucket } = this.props;

        this.state = {
            posts: null,
            bucket: bucket
        }
    }

    componentDidMount () {
        this.fetchPosts()
    }

    // this needs rebuilding - is returning null more often than not before the async function has resolved
    async fetchPosts(){
        await Storage.list('', { level: "public", bucket: this.state.bucket })
        .then( async result => {
            console.log('feed result', result);
    
            let list = async () => {
                let arr = [];
                
                await result.forEach(async e => {
                    var directory = false;
                    const isDir = e.key.split("/");
        
                    if (!isDir[1]){
                      directory = true;
                    }
                        
                    if (e.key !== ""){
                    console.log('key', e.key)
                    await Storage.get(e.key, { level: "public", bucket: this.state.bucket })
                        .then(post => {
                            console.log('post', post);
                            arr[arr.length] = {url : post};
                        });
                    }
                });

                return arr; // this is probably returning a null value, is not awaiting the response
            };
    
            await list().then((value) => this.setState({posts: value}))
        })
        .catch(err => console.log(err));
    }
    
    render() {
        if (this.state.posts){
            console.log(this.state.posts);
            
            return (
                <div className="list shadow radius white">
                    { this.state.posts.map(post => (
                        <div className="item">

                            <div className="item full">
                                <video width="100%">
                                    <source src={post.url} />
                                </video>
                            </div>

                            <div className="item text-grey-600">
                              Emoji voting here
                            </div>

                            <a href={post.url}>Download</a>
                        </div>
                    )) }
                </div>
            );
            /* 
            .forEach(post => {
                console.log(post);
                const videoJsOptions = {
                    autoplay: false,
                    controls: true,
                    sources: [{
                      src: post.url,//window.URL.createObjectURL(uri),
                      type: 'video/webm'
                    }]
                };
                return (
                    <div>
                        <VideoPlayer { ...videoJsOptions } />
                        <i href={post.url}>Download</i>
                    </div>
                )
            }); */
        }
        else{
            return (
                <div>Loading...</div>
            );
        }
    }
}

export default Feed;