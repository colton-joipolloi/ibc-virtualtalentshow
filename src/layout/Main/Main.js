import React, { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Header from '../../components/Header/Header';
import Feed from '../../pages/Feed/Feed.lazy';
import SelectAudio from '../../pages/SelectAudio/SelectAudio.lazy';
import VideoSubmitted from '../../pages/VideoSubmitted/VideoSubmitted.lazy';
import Record from '../../pages/Record/Record.lazy';
import PendingPosts from '../../pages/PendingPosts/PendingPosts.lazy';
import NotFound from '../../pages/NotFound/NotFound.lazy';
import Start from '../../pages/Start/Start.lazy';
import SignOut from '../../pages/SignOut/SignOut.lazy';
import VideoEdit from '../../pages/VideoEdit/VideoEdit.lazy';
import { ReactComponent as Logo } from './icons/logo.svg';
import { API, graphqlOperation } from 'aws-amplify';
import * as queries from '../../graphql/queries';

import './Main.scss';
import Forbidden from '../../pages/Forbidden/Forbidden.lazy';
import VideoRecord from '../../pages/VideoRecord/VideoRecord.lazy';
import { useDispatch } from 'react-redux';
import { setLiveShow } from '../../store/state/actions';
import Vote from '../../components/Vote/Vote';
import { ReactComponent as Mic } from './icons/sing.svg';
import { ReactComponent as ArrowDown } from './icons/arrow-down.svg';
import { ReactComponent as ArrowUp } from './icons/arrow-up.svg';
import { useCookies } from 'react-cookie';
import StartLive from '../../pages/StartLive/StartLive.lazy';

const Main = ({ authState }) => {
    const [cookies, setCookie] = useCookies(['instructions']);
    const reduxDispatch = useDispatch();
    useEffect(() => {
        async function fetchLiveShow() {
            const config = await API.graphql(
                graphqlOperation(queries.getConfig, {
                    id: 'a47a7ca6-7cf4-4f0a-a6c0-3280b0486021',
                })
            );
            reduxDispatch(setLiveShow(config.data.getConfig));
        }
        if (authState === 'signedIn') {
            fetchLiveShow();
            setInterval(() => {
                fetchLiveShow();
            }, 30000);
        }
    }, [authState, reduxDispatch]);

    const closeInstructions = useCallback(() => {
        setCookie('instructions', true, { path: '/' });
    }, [setCookie]);

    if (authState !== 'signedIn') {
        return null;
    }

    if (!window.MediaRecorder) {
        return (
            <div className='Main'>
                <div className='Main__Device_Support'>
                    <Logo></Logo>
                    <h2>Unsupported device or browser</h2>
                    <p>Please use Google Chrome on Desktop or Android</p>
                </div>
            </div>
        )
    }
        
    return (
        <div className='Main'>
            <Router>
                <Switch>
                    <Route exact path='/'>
                        <Header />
                        <Feed />
                    </Route>
                    <Route exact path='/start'>
                        <Header />
                        <Start />
                    </Route>
                    <Route exact path='/start-live'>
                        <Header />
                        <StartLive />
                    </Route>
                    <Route exact path='/select'>
                        <Header />
                        <SelectAudio />
                    </Route>
                    <Route exact path='/record'>
                        <Header back={true} />
                        <Record />
                    </Route>
                    <Route exact path='/test'>
                        <VideoRecord />
                    </Route>
                    <Route exact path='/edit'>
                        <VideoEdit />
                    </Route>
                    <Route exact path='/submitted'>
                        <VideoSubmitted />
                    </Route>
                    <Route exact path='/post/pending'>
                        <Header />
                        <PendingPosts />
                    </Route>
                    <Route exact path='/403'>
                        <Header />
                        <Forbidden />
                    </Route>
                    <Route exact path='/signout'>
                        <SignOut />
                    </Route>
                    <Route component={NotFound}></Route>
                </Switch>
            </Router>
            {!cookies.instructions && (
                <div className='Main__Instructions' onClick={closeInstructions}>
                    <div className='Main__Instructions__Vote'>
                        <Vote />
                    </div>
                    <div className='Main__Instructions__Swipe'>
                        <ArrowUp />
                        <div>
                            Swipe up and down to
                            <br />
                            view singers
                        </div>
                        <ArrowDown />
                    </div>
                    <div className='Main__Instructions__Arrow React'>
                        <div>Click to react</div>
                        <ArrowDown />
                    </div>
                    <div className='Main__Instructions__Arrow Record'>
                        <div>Click to record</div>
                        <ArrowDown />
                    </div>
                    <div className='Main__Instructions__Record'>
                        <Mic />
                    </div>
                </div>
            )}

            <div className='Main__Rotate'>
                <Logo></Logo>
                <h2>Please rotate your device</h2>
            </div>
        </div>
    );
};

Main.propTypes = {
    authState: PropTypes.string,
};

Main.defaultProps = {};

export default Main;
