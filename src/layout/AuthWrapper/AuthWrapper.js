import React, { useEffect, useCallback } from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { Auth } from 'aws-amplify';
import CustomSignIn from '../../components/CustomSignIn/CustomSignIn';
import Main from '../Main/Main';
import { setUser } from '../../store/state/actions';

const AuthWrapper = ({ authState, onStateChange }) => {
    const dispatch = useDispatch();

    const updateUser = useCallback(
        (user) => {
            dispatch(setUser(user));
        },
        [dispatch]
    );

    useEffect(() => {
        async function getUser() {
            try {
                const user = await Auth.currentAuthenticatedUser();
                const currentSession = user.signInUserSession;
                user.refreshSession(
                    currentSession.refreshToken,
                    (err, session) => {
                        updateUser(session.idToken.payload);
                    }
                );
            } catch (err) {
                console.log('error getting user: ', err);
            }
        }
        getUser();
    }, [updateUser]);

    return (
        <div>
            <CustomSignIn
                authState={authState}
                updateUser={updateUser}
                onStateChange={onStateChange}
            />
            <Main authState={authState} onStateChange={onStateChange} />
        </div>
    );
};

AuthWrapper.propTypes = {
    authState: PropTypes.string,
    onStateChange: PropTypes.func,
};

AuthWrapper.defaultProps = {};

export default AuthWrapper;
