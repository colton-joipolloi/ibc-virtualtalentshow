export const stripExtension = (src, newExt = '') => {
    const lastDotIndex = src.lastIndexOf('.');
    return `${src.substr(0, lastDotIndex)}${newExt}`;
};

export const fileName = (src) => {
    const lastPart = src.substring(src.lastIndexOf('/') + 1).replace(/_/g, ' ');
    return stripExtension(lastPart);
};

export const videoOptions = () => {
    const MIME_TYPES = [
        'video/webm;codecs="vp8,opus"',
        'video/webm;codecs=h264',
        'video/webm;codecs=vp9',
        'video/webm',
    ];

    let options;
    if (!window.MediaRecorder.isTypeSupported) {
        options = { mimeType: 'video/mp4; codecs=h264' };
    } else {
        if (window.MediaRecorder.isTypeSupported('video/mp4;codecs=h264')) {
            options = { mimeType: 'video/mp4; codecs=h264' };
        } else if (
            window.MediaRecorder.isTypeSupported('video/mov;codecs=h264')
        ) {
            options = { mimeType: 'video/mov; codecs=h264' };
        } else {
            options = {
                mimeType: window.MediaRecorder.isTypeSupported
                    ? MIME_TYPES.find(window.MediaRecorder.isTypeSupported)
                    : 'video/webm',
            };
        }
    }

    return options;
};
