import React, { lazy, Suspense } from 'react';
import Loading from '../../components/Loading/Loading';

const LazyNotFound = lazy(() => import('./NotFound'));

const NotFound = (props) => (
    <Suspense fallback={<Loading />}>
        <LazyNotFound {...props} />
    </Suspense>
);

export default NotFound;
