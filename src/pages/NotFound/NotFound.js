import React from 'react';
import PropTypes from 'prop-types';
import './NotFound.scss';

const NotFound = () => (
    <div className='NotFound'>
        <h1>Page not found</h1>
    </div>
);

NotFound.propTypes = {};

NotFound.defaultProps = {};

export default NotFound;
