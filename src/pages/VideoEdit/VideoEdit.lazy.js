import React, { lazy, Suspense } from 'react';

const LazyVideoEdit = lazy(() => import('./VideoEdit'));

const VideoEdit = props => (
  <Suspense fallback={null}>
    <LazyVideoEdit {...props} />
  </Suspense>
);

export default VideoEdit;
