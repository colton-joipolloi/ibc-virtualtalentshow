import React, { useCallback, useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { selectMusic } from '../../store/music/selectors';
import { selectUsername, selectUserGroups } from '../../store/state/selectors';
import PropTypes from 'prop-types';
import { ReactComponent as Retry } from './icons/retry.svg';
import API, { graphqlOperation } from '@aws-amplify/api';
import { Storage } from 'aws-amplify';
import * as mutations from '../../graphql/mutations';
import { videoOptions } from '../../util';
import { withRouter } from 'react-router-dom';
import ProgressBar from '../../components/ProgressBar/ProgressBar';
import { setAudioTrack, resetRecording } from '../../store/music/actions';
import PlyrEditor from '../../components/PlyrEditor/PlyrEditor';

import './VideoEdit.scss';

const options = videoOptions();

const VideoEdit = ({ history }) => {
    const [progress, setProgress] = useState(0);
    const [uploading, setUploading] = useState(false);
    const [rewind, setRewind] = useState(false);
    const {
        selectedTrack,
        videoDuration,
        videoUrl,
        videoBlob,
        videoStart,
        videoEnd,
    } = useSelector(selectMusic);
    const username = useSelector(selectUsername);
    const groups = useSelector(selectUserGroups);
    const finalist = groups.includes('finalist');
    const reduxDispatch = useDispatch();

    useEffect(() => {
        if (!selectedTrack.url) {
            history.push('/select');
        }
    }, [history, selectedTrack.url]);

    const reset = useCallback(() => {
        reduxDispatch(resetRecording());
        history.push('./record');
    }, [history, reduxDispatch]);

    const handleRewind = useCallback((value) => {
        setRewind(value);
    }, []);

    const uploadFile = useCallback(() => {
        setUploading(true);
        const file = videoBlob;
        let ext = options.mimeType.split('/');
        ext = ext[1].split(';');
        ext = '.' + ext[0];

        const name = Math.random().toString(36).slice(2) + ext;
        async function upload() {
            try {
                const putFile = await Storage.put(name, file, {
                    contentType: options.mimeType,
                    level: 'public',
                    bucket: finalist
                        ? 'live-raw-uploads-develop'
                        : 'raw-uploads145703-develop',
                    metadata: {
                        username: username,
                        track: selectedTrack.track,
                        artist: selectedTrack.artist,
                        category: selectedTrack.category,
                        startTime: videoStart.toString(),
                        endTime: videoEnd.toString(),
                    },
                    progressCallback(progress) {
                        if (progress) {
                            setProgress(
                                (progress.loaded / progress.total) * 100
                            );
                        }
                    },
                });

                const split = putFile.key.split('.');
                const videoS3Key = split[0];

                // pesist a record with the s3 key id & the user that submitted it (user is automatically stored via the Amplify magic)
                API.graphql(
                    graphqlOperation(mutations.createPost, {
                        input: {
                            id: videoS3Key,
                            userName: username,
                            trackName: selectedTrack.track,
                            artist: selectedTrack.artist,
                        },
                    })
                );

                history.push('/submitted');

                reduxDispatch(
                    setAudioTrack({
                        url: null,
                        track: null,
                        artist: null,
                        category: null,
                    })
                );
            } catch (error) {
                console.log(error.response);
            }
        }
        upload();
    }, [
        finalist,
        history,
        reduxDispatch,
        selectedTrack.artist,
        selectedTrack.category,
        selectedTrack.track,
        username,
        videoBlob,
        videoEnd,
        videoStart,
    ]);

    return (
        <div className='VideoEdit'>
            {uploading ? (
                <ProgressBar
                    progress={progress}
                    size={200}
                    strokeWidth={10}
                    circleOneStroke='#ffffff'
                    circleTwoStroke='#00f3ff'
                />
            ) : (
                <>
                    <div className='VideoEdit__Preview'>
                        <PlyrEditor
                            src={videoUrl}
                            duration={videoDuration}
                            start={videoStart}
                            end={videoEnd}
                            selectedTrack={selectedTrack}
                            username={username}
                        ></PlyrEditor>
                    </div>
                    <div className='VideoEdit__Submit'>
                        <button
                            className='btn btn-solid-blue'
                            onClick={uploadFile}
                        >
                            Submit
                        </button>
                        <button
                            className='VideoEdit__Retry'
                            onClick={() => handleRewind(true)}
                        >
                            <Retry />
                        </button>
                    </div>
                </>
            )}
            {rewind && (
                <div className='VideoEdit__Confirm'>
                    <div className='VideoEdit__Confirm__Modal'>
                        <div className='VideoEdit__Confirm__Content'>
                            <h3>Are you sure?</h3>
                            <p>
                                Doing this will erase your current video. Do you
                                want to record again?
                            </p>
                        </div>
                        <div className='VideoEdit__Confirm__Buttons'>
                            <button
                                className='btn btn-solid-blue'
                                onClick={reset}
                            >
                                Yes
                            </button>
                            <button
                                className='btn btn-solid-blue-outline'
                                onClick={() => handleRewind(false)}
                            >
                                No
                            </button>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
};

VideoEdit.propTypes = {};

VideoEdit.defaultProps = {};

export default withRouter(VideoEdit);
