import React, { useRef, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import './VideoRecord.scss';
import { useUserMedia } from '../../hooks/use-user-media';
import { classNames } from 'classnames';

const CAPTURE_OPTIONS = {
    audio: true,
    video: { aspectRatio: 0.5625 },
};

const Camera = () => {
    const videoRef = useRef();
    const mediaStream = useUserMedia(CAPTURE_OPTIONS);

    useEffect(() => {
        if (videoRef.current) {
            videoRef.current.srcObject = mediaStream;
            const options = { mimeType: 'video/webm' };
            const mediaRecorder = new MediaRecorder(mediaStream, options);

            mediaRecorder.addEventListener('dataavailable', function (e) {
                console.log(e);
                if (e.data.size > 0) {
                    console.log(e.data);
                }
            });

            mediaRecorder.start();

            setTimeout(() => {
                mediaRecorder.stop();
            }, 5000);
        }
    }, [mediaStream]);

    console.log(mediaStream);

    if (!mediaStream) {
        return null;
    }

    return (
        <div className='VideoRecord'>
            <video
                style={{ width: '100%', height: '100%' }}
                autoPlay
                playsInline
                muted
                ref={videoRef}
            ></video>
        </div>
    );
};

const VideoRecord = () => {
    const [isCameraOpen, setIsCameraOpen] = useState(false);

    return (
        <div className='VideoRecord'>
            {!isCameraOpen ? (
                <button className='btn' onClick={() => setIsCameraOpen(true)}>
                    Open Camera
                </button>
            ) : (
                <Camera />
            )}
        </div>
    );
};

VideoRecord.propTypes = {};

VideoRecord.defaultProps = {};

export default VideoRecord;
