import React, { lazy, Suspense } from 'react';

const LazyVideoRecord = lazy(() => import('./VideoRecord'));

const VideoRecord = props => (
  <Suspense fallback={null}>
    <LazyVideoRecord {...props} />
  </Suspense>
);

export default VideoRecord;
