import React from 'react';
import { withRouter } from 'react-router-dom';
import { ReactComponent as BigTick } from './icons/big-tick.svg';
import './VideoSubmitted.scss';

const VideoSubmitted = ({ history }) => (
    <div className='VideoSubmitted'>
        <div className='VideoSubmitted__Content'>
            <BigTick />
            <h1> Video submitted </h1>
            <p>
                Thank you for submitting your video. Check back in xx days to
                see if you’ve been selected for the live show.
            </p>
            <button
                className='btn btn-solid-blue'
                onClick={() => history.push('/')}
            >
                Watch other people's performances
            </button>
        </div>
    </div>
);

export default withRouter(VideoSubmitted);
