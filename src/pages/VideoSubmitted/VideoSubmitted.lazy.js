import React, { lazy, Suspense } from 'react';
import Loading from '../../components/Loading/Loading';

const LazyVideoSubmitted = lazy(() => import('./VideoSubmitted'));

const VideoSubmitted = (props) => (
    <Suspense fallback={<Loading />}>
        <LazyVideoSubmitted {...props} />
    </Suspense>
);

export default VideoSubmitted;
