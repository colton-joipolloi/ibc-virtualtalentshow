import React, { lazy, Suspense } from 'react';

const LazyStartLive = lazy(() => import('./StartLive'));

const StartLive = props => (
  <Suspense fallback={null}>
    <LazyStartLive {...props} />
  </Suspense>
);

export default StartLive;
