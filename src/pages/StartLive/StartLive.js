import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { ReactComponent as Headphones } from './icons/headphones.svg';
import { selectUserGroups, selectIsLive } from '../../store/state/selectors';
import { withRouter, Link } from 'react-router-dom';
import './StartLive.scss';

const StartLive = ({ history }) => {
    const groups = useSelector(selectUserGroups);
    const isLive = useSelector(selectIsLive);
    const finalist = groups.includes('finalist');

    // useEffect(() => {
    //     if (!finalist || isLive) {
    //         history.push('./403');
    //     }
    // }, [finalist, history, isLive]);

    return (
        <div className='StartLive'>
            <div className='StartLive__Content'>
                <Headphones className='StartLive__Icon' />
                <h1>Ready to begin?</h1>
                <Link
                    className='btn btn-solid-blue StartLive__Continue'
                    to='/select'
                >
                    Record my final performance
                </Link>
            </div>
        </div>
    );
};

StartLive.propTypes = { history: PropTypes.object };

StartLive.defaultProps = {};

export default withRouter(StartLive);
