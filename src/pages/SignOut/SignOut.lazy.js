import React, { lazy, Suspense } from 'react';

const LazyStart = lazy(() => import('./SignOut'));

const SignOut = props => (
  <Suspense fallback={null}>
    <LazyStart {...props} />
  </Suspense>
);

export default SignOut;
