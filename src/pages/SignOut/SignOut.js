import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { withRouter, Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { Auth } from 'aws-amplify';

import './SignOut.scss';

const SignOut = ({ history }) => {

    return (
        <div className='Start'>
            <div className='Start__Content'>
                <h1>Profile</h1>
                <p>
                    Are you sure you want to
                    <br />
                    logout?
                </p>
                    <button
                        className='btn'
                        onClick={ () => {
                            history.goBack();
                        }}
                    >
                    <i className=''></i> Back
                    </button>

                    <br />

                    <button
                        className='btn btn-solid-blue'
                        onClick={ () => {
                            Auth.signOut();
                            history.push('/');
                        }}
                    >
                        <i className=''></i> Logout
                    </button>
            </div>
        </div>
    );
};

SignOut.propTypes = { history: PropTypes.object };

SignOut.defaultProps = {};

export default withRouter(SignOut);
