import React, { useState, useEffect } from 'react';
import { Storage } from 'aws-amplify';
import Loading from '../../components/Loading/Loading';
import { withRouter } from 'react-router-dom';

import './PendingPosts.scss';

const PendingPosts = ({ history }) => {
    const [posts, setPosts] = useState(0);

    useEffect(() => {
        async function fetchPosts() {
            try {
                const items = await Storage.list('uploads', {
                    level: 'protected',
                });

                let postsArr = [];

                async function getPosts() {
                    for (const item of items) {
                        const post = await Storage.get(item.key, {
                            level: 'protected',
                        });
                        if (post) {
                            postsArr = [
                                ...postsArr,
                                { key: item.key, url: post },
                            ];
                        }
                    }
                }

                await getPosts();
                setPosts(postsArr);
            } catch (error) {
                if (error.response && error.response.status === 403) {
                    history.push('/403');
                }
            }
        }
        fetchPosts();
    }, [history]);

    if (!posts) {
        return <Loading />;
    }
    return (
        <div className='PendingPosts'>
            {posts.map((post) => (
                <a href={post.url}>Download</a>
            ))}
        </div>
    );
};

PendingPosts.propTypes = {};

PendingPosts.defaultProps = {};

export default withRouter(PendingPosts);
