import React, { lazy, Suspense } from 'react';
import Loading from '../../components/Loading/Loading';

const LazyPendingPosts = lazy(() => import('./PendingPosts'));

const PendingPosts = (props) => (
    <Suspense fallback={<Loading />}>
        <LazyPendingPosts {...props} />
    </Suspense>
);

export default PendingPosts;
