import React, { useReducer, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { selectMusic } from '../../store/music/selectors';

import KareokeRecorder from '../../components/KareokeRecorder/KareokeRecorder';
import KareokeRecorderActions from '../../components/KareokeRecorder/Actions';
import SelectInput from '../../components/KareokeRecorder/SelectInput';
import ToggleEcho from '../../components/KareokeRecorder/ToggleEcho';
import MicVisualizer from '../../components/KareokeRecorder/MicVisualizer';

import { withRouter } from 'react-router-dom';

import { setRecording } from '../../store/music/actions';
import { selectUsername } from '../../store/state/selectors';
import VideoHeader from '../../components/VideoHeader/VideoHeader';
import { videoOptions } from '../../util';

import './Record.scss';

const initialState = {
    status: 'recording_perms',
};

const reducer = (state, action) => {
    if (action.type === 'setStatus') {
        return { ...state, status: action.payload };
    }
};

const options = videoOptions();

const Record = ({ history }) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const { selectedTrack, videoBlob } = useSelector(selectMusic);
    const username = useSelector(selectUsername);
    const reduxDispatch = useDispatch();

    // do we want the enable the microphone "echo" whilst recording
    const [micPlayback, setIsMicPlayback] = React.useState(false);
   
    useEffect(() => {
        if (!selectedTrack.url) {
            history.push('/select');
        }
    }, [history, selectedTrack.url]);

    useEffect(() => {
        if (videoBlob) {
            history.push('./edit');
        }
    }, [history, videoBlob]);

    const record = (videoBlob) => {
        reduxDispatch(setRecording(videoBlob));
    };

    const setStatus = (status) => {
        dispatch({ type: 'setStatus', payload: status });
    };

    return (
        <div className='Record'>
            {state.status !== 'complete' && (
                <MicVisualizer />
            )}
            {state.status === 'start' && (
                <div className='Record__Start'>
                    <div className='Record__Start__Content'>
                        <p>
                            When you’re ready, press the record button and the
                            timer will count you down
                        </p>
                        <p>
                            <strong>Remember, headphones on!</strong>
                        </p>
                    </div>
                    <button
                        className='btn btn-solid-blue'
                        onClick={() => {
                            setStatus('get_ready');
                        }}
                    >
                        Ok
                    </button>
                </div>
            )}

            <div className='Record__Recorder'>
                <div className='Record__Info'>
                    <VideoHeader
                        username={username}
                        track={selectedTrack.track}
                        artist={selectedTrack.artist}
                    />
                    <ToggleEcho 
                        status={state.status}
                        microphonePlayback={micPlayback}
                        toggleEcho={setIsMicPlayback}
                    />
                </div>
                {state.status !== 'complete' && (
                    <>
                        <KareokeRecorder
                            mediaRecorderOpts={options}
                            isOnInitially={true}
                            isFlipped={true}
                            showReplayControls={true}
                            replayVideoAutoplayAndLoopOff={true}
                            renderActions={KareokeRecorderActions}
                            selectedAudio={selectedTrack}
                            microphonePlayback={micPlayback}
                            style={{ height: '100vh' }}
                            dataAvailableTimeout={10000}
                            constraints={{
                                audio: true,
                                video: { aspectRatio: 0.5625 },
                            }}
                            onCameraOn={() => {
                                setStatus('start');
                            }}
                            onStartRecording={() => {
                                setStatus('recording');
                            }}
                            onStopReplaying={() => {
                                setStatus(null);
                            }}
                            onRecordingComplete={(videoBlob) => {
                                setStatus('complete');
                                record(videoBlob);
                            }}
                        />
                        <SelectInput />
                    </>
                )}
            </div>
        </div>
    );
};

export default withRouter(Record);
