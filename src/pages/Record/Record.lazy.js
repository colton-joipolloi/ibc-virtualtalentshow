import React, { lazy, Suspense } from 'react';
import Loading from '../../components/Loading/Loading';

const LazyRecord = lazy(() => import('./Record'));

const Record = (props) => (
    <Suspense fallback={<Loading />}>
        <LazyRecord {...props} />
    </Suspense>
);

export default Record;
