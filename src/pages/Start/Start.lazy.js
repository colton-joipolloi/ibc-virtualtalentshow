import React, { lazy, Suspense } from 'react';

const LazyStart = lazy(() => import('./Start'));

const Start = props => (
  <Suspense fallback={null}>
    <LazyStart {...props} />
  </Suspense>
);

export default Start;
