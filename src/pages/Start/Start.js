import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { ReactComponent as Headphones } from './icons/headphones.svg';
import { ReactComponent as Calendar } from './icons/calendar.svg';
import { withRouter, Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import {
    selectUserGroups,
    selectIsLive,
    selectLiveDate,
} from '../../store/state/selectors';
import { format } from 'date-fns';
import './Start.scss';

const Start = ({ history }) => {
    const groups = useSelector(selectUserGroups);
    const isLive = useSelector(selectIsLive);
    const liveDate = useSelector(selectLiveDate);
    const formatDate = format(new Date(liveDate), 'd MMMM yyyy');
    const formatTime = format(new Date(liveDate), 'kk:mm');
    const finalist = groups.includes('finalist');

    // useEffect(() => {
    //     if (isLive) {
    //         history.push('./403');
    //     }
    // }, [history, isLive]);

    return (
        <div className='Start'>
            <div className='Start__Content'>
                {/* {finalist && !isLive && (
                    <>
                        <Calendar className='Start__Icon' />
                        <h1>
                            Please come back between
                            <br />
                            <span className='green'>
                                {formatTime} on {formatDate}
                            </span>
                            <br />
                            to record your performance.
                        </h1>
                    </>
                )} */}

                <Headphones className='Start__Icon' />
                <h1>Ready to record your performance?</h1>
                <p>
                    Find somewhere quiet and get your
                    <br />
                    headphones on.
                </p>
                <Link
                    className='btn btn-solid-blue Start__Continue'
                    to='/select'
                >
                    Continue
                </Link>
            </div>
        </div>
    );
};

Start.propTypes = { history: PropTypes.object };

Start.defaultProps = {};

export default withRouter(Start);
