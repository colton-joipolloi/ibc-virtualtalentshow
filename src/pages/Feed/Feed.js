import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Storage } from 'aws-amplify';
import Loading from '../../components/Loading/Loading';
import FeedPost from '../../components/FeedPost/FeedPost';
import { ReactComponent as Mic } from './icons/sing.svg';
import { Link } from 'react-router-dom';
import { selectUserGroups } from '../../store/state/selectors';
import { API, graphqlOperation } from 'aws-amplify';
import * as queries from '../../graphql/queries';
import { selectFeedPosts } from '../../store/feed/selectors';
import { setFeedPosts } from '../../store/feed/actions';
import FeedPostDemo from '../../components/FeedPostDemo/FeedPostDemo';

import example1 from './video/example1.mp4';
import example2 from './video/example2.mp4';

import './Feed.scss';

const Feed = () => {
    const posts = useSelector(selectFeedPosts);
    const groups = useSelector(selectUserGroups);
    const finalist = groups.includes('finalist');
    const reduxDispatch = useDispatch();
    const bucket = 'transcoded-uploads153015-develop';

    const startPage = finalist ? '/start-live' : '/start';

    useEffect(() => {
        async function fetchPosts() {
            const items = await Storage.list('', {
                level: 'public',
                bucket: bucket,
            });

            let postsArr = [];

            async function getPosts() {
                for (const item of items) {
                    if (item.key !== '') {
                        const split = item.key.split('/');
                        const s3Key = split[0];
                        const file = split[1];

                        if (file === 'web.mp4') {
                            const post = await Storage.get(item.key, {
                                level: 'public',
                                bucket: bucket,
                            });

                            const postData = await API.graphql(
                                graphqlOperation(queries.getPost, { id: s3Key })
                            );

                            if (post) {
                                postsArr = [
                                    ...postsArr,
                                    {
                                        key: s3Key,
                                        url: post,
                                        data: postData.data.getPost,
                                    },
                                ];
                            }
                        }
                    }
                }
            }

            await getPosts();
            reduxDispatch(setFeedPosts(postsArr));
        }
        fetchPosts();
    }, [reduxDispatch]);

    if (!posts) {
        return <Loading />;
    }

    return (
        <div className='Feed'>
            {posts.length &&
                posts.map((post, i) => (
                    <FeedPost key={post.key} post={post} first={i === 0} />
                ))}
            {!posts.length && (
                <>
                    <FeedPostDemo src={example1} first={true} />
                    <FeedPostDemo src={example2} first={false} />
                </>
            )}
            <div className='Feed__Grad'></div>
            <Link className='Feed__Select' to={startPage}>
                <Mic />
            </Link>
        </div>
    );
};

export default Feed;
