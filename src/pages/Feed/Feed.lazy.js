import React, { lazy, Suspense } from 'react';
import Loading from '../../components/Loading/Loading';

const LazyFeed = lazy(() => import('./Feed'));

const Feed = (props) => (
    <Suspense fallback={<Loading />}>
        <LazyFeed {...props} />
    </Suspense>
);

export default Feed;
