import React, { useReducer, useEffect, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Storage } from 'aws-amplify';
import Loading from '../../components/Loading/Loading';
import { setAudioTrack } from '../../store/music/actions';
import ReactHowler from 'react-howler';
import { selectSelectedTrack } from '../../store/music/selectors';
import { withRouter } from 'react-router-dom';
import classNames from 'classnames';
import { fileName } from '../../util';
import { ReactComponent as Plus } from './icons/plus.svg';
import { ReactComponent as Tick } from './icons/tick.svg';
import './SelectAudio.scss';
import PlayBtn from '../../components/PlayBtn/PlayBtn';

const initialState = {
    playing: false,
    loading: false,
    choosen: false,
    selectedCategory: null,
    tracks: [],
    categories: [],
};

const reducer = (state, action) => {
    if (action.type === 'setPlaying') {
        return { ...state, playing: action.payload, loading: action.payload };
    }
    if (action.type === 'setChoosen') {
        return { ...state, choosen: action.payload };
    }
    if (action.type === 'setLoading') {
        return { ...state, loading: action.payload };
    }
    if (action.type === 'setCategory') {
        return {
            ...state,
            selectedCategory: action.payload,
            playing: false,
            loading: false,
            choosen: false,
        };
    }
    if (action.type === 'setTracks') {
        return {
            ...state,
            tracks: action.payload.tracks,
            categories: action.payload.categories,
            selectedCategory: action.payload.categories[0].dir,
        };
    }
};

const SelectAudio = ({ history }) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const selectedTrack = useSelector(selectSelectedTrack);
    const reduxDispatch = useDispatch();
    const bucket = 'audio-tracks-153015';

    const updateAudioTrack = useCallback(
        (trackMetaData) => {
            reduxDispatch(setAudioTrack(trackMetaData));
        },
        [reduxDispatch]
    );

    useEffect(() => {
        async function fetchTracks() {
            try {
                const items = await Storage.list('', {
                    level: 'public',
                    bucket: bucket,
                });

                let trackArr = [];
                let categoryArr = [];

                async function getTracks() {
                    for (const item of items) {
                        let directory = item.size === 0;
                        const folders = item.key.split('/');

                        if (item.key !== '' && directory !== true) {
                            const post = await Storage.get(item.key, {
                                level: 'public',
                                bucket: bucket,
                            });
                            if (post) {
                                trackArr = [
                                    ...trackArr,
                                    {
                                        track: fileName(item.key),
                                        url: post,
                                        category: folders[0],
                                        artist: folders[1],
                                    },
                                ];
                            }
                        } else if (item.key !== '' && folders.length === 2) {
                            categoryArr = [
                                ...categoryArr,
                                {
                                    dir: folders[0],
                                },
                            ];
                        }
                    }
                }

                await getTracks();
                dispatch({
                    type: 'setTracks',
                    payload: { tracks: trackArr, categories: categoryArr },
                });
                updateAudioTrack({
                    url: null,
                    track: null,
                    artist: null,
                    category: null,
                });
            } catch (error) {
                console.log(error.response);
            }
        }
        fetchTracks();
    }, [updateAudioTrack]);

    const handlePlay = useCallback(
        (value) => {
            // selectedTrack.track === value.track
            // dispatch({ type: 'setPlaying', payload: playing });
            dispatch({ type: 'setChoosen', payload: false });
            if (selectedTrack.track !== value.track) {
                dispatch({ type: 'setLoading', payload: true });
                dispatch({ type: 'setPlaying', payload: true });
                updateAudioTrack({
                    url: value.url,
                    track: value.track,
                    artist: value.artist,
                    category: value.category,
                });
            } else {
                dispatch({ type: 'setPlaying', payload: false });
                updateAudioTrack({
                    url: null,
                    track: null,
                    artist: null,
                    category: null,
                });
            }
        },
        [selectedTrack.track, updateAudioTrack]
    );

    const handleChoosen = useCallback(
        (value) => {
            // selectedTrack.track === value.track
            // dispatch({ type: 'setPlaying', payload: playing });
            if (!state.choosen) {
                dispatch({ type: 'setChoosen', payload: true });
                dispatch({ type: 'setPlaying', payload: false });
                updateAudioTrack({
                    url: value.url,
                    track: value.track,
                    artist: value.artist,
                    category: value.category,
                });
            } else {
                dispatch({ type: 'setChoosen', payload: false });
                updateAudioTrack({
                    url: null,
                    track: null,
                    artist: null,
                    category: null,
                });
            }
        },
        [state.choosen, updateAudioTrack]
    );

    const setCategory = (cat) => {
        dispatch({ type: 'setCategory', payload: cat });
        updateAudioTrack({
            url: null,
            track: null,
            artist: null,
            category: null,
        });
    };

    const handleLoad = (loading) => {
        dispatch({ type: 'setLoading', payload: loading });
    };

    const selector = () => {
        return (
            <>
                {state.categories.map((value, index) => {
                    // console.log(value, index);
                    const classes = classNames('btn btn-slim', {
                        selected: state.selectedCategory === value.dir,
                    });
                    return (
                        <button
                            key={index}
                            className={classes}
                            onClick={() => setCategory(value.dir)}
                        >
                            {value.dir.charAt(0).toUpperCase() +
                                value.dir.slice(1)}
                        </button>
                    );
                })}
            </>
        );
    };

    const tracks = () => {
        const categoryTracks = state.tracks.filter(
            (t) => t.category === state.selectedCategory
        );
        return (
            <div className='SelectAudio__TrackList'>
                {categoryTracks.map((value, index) => {
                    const classes = classNames('SelectAudio__Track', {
                        selected: selectedTrack.url === value.url,
                        choosen:
                            selectedTrack.url === value.url && state.choosen,
                        muted: selectedTrack.url !== value.url && state.choosen,
                    });

                    return (
                        <div key={index} className={classes}>
                            <div className='SelectAudio__Track__Play'>
                                <button onClick={() => handlePlay(value)}>
                                    <PlayBtn
                                        loading={
                                            selectedTrack.track ===
                                                value.track &&
                                            state.loading &&
                                            !state.choosen
                                        }
                                        playing={
                                            selectedTrack.track ===
                                                value.track && !state.choosen
                                        }
                                    ></PlayBtn>
                                </button>
                            </div>
                            <div className='SelectAudio__Track__Content'>
                                <h2>{value.track}</h2>
                                <span className='artist'>{value.artist}</span>
                            </div>
                            <button
                                className='SelectAudio__Track__Selector'
                                onClick={() => handleChoosen(value)}
                            >
                                {selectedTrack.url === value.url &&
                                state.choosen ? (
                                    <Tick />
                                ) : (
                                    <>
                                        <br></br>
                                        <Plus />
                                        <h4> Select </h4>
                                    </>
                                )}
                            </button>
                        </div>
                    );
                })}
            </div>
        );
    };

    if (!state.tracks.length) {
        return <Loading />;
    }

    return (
        <div className='SelectAudio'>
            <div className='SelectAudio__Filter'>
                <h1>Choose your song</h1>
                <div className='SelectAudio__Options'>{selector()}</div>
            </div>
            {tracks()}
            {selectedTrack.track && selectedTrack.url && (
                <>
                    <ReactHowler
                        playing={
                            selectedTrack.track &&
                            selectedTrack.url &&
                            state.playing
                        }
                        src={selectedTrack.url}
                        onLoad={() => handleLoad(true)}
                        onPlay={() => handleLoad(false)}
                    />
                    {state.choosen && (
                        <button
                            className='btn btn-solid-blue SelectAudio__Record'
                            onClick={() => history.push('/record')}
                        >
                            Next
                        </button>
                    )}

                    {/* <div className='SelectAudio__SelectedTrack'>
                        {state.loading ? (
                            <div className='SelectAudio__SelectedTrack__Name'>
                                Loading....
                            </div>
                        ) : (
                            <div className='SelectAudio__SelectedTrack__Name'>
                                {selectedTrack.track}
                                <br />
                                {selectedTrack.category}
                            </div>
                        )}

                        <button
                            className='SelectAudio__SelectedTrack__Record'
                            onClick={() => history.push('/record')}
                        >
                            <i className='icon ion-videocamera'></i>
                            Record
                        </button>
                    </div> */}
                </>
            )}
        </div>
    );
};

export default withRouter(SelectAudio);
