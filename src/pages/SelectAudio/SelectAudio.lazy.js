import React, { lazy, Suspense } from 'react';
import Loading from '../../components/Loading/Loading';

const LazySelectAudio = lazy(() => import('./SelectAudio'));

const SelectAudio = (props) => (
    <Suspense fallback={<Loading />}>
        <LazySelectAudio {...props} />
    </Suspense>
);

export default SelectAudio;
