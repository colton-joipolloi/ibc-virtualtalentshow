import React, { lazy, Suspense } from 'react';
import Loading from '../../components/Loading/Loading';

const LazyForbidden = lazy(() => import('./Forbidden'));

const Forbidden = (props) => (
    <Suspense fallback={<Loading />}>
        <LazyForbidden {...props} />
    </Suspense>
);

export default Forbidden;
