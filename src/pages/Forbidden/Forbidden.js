import React from 'react';
import PropTypes from 'prop-types';
import { ReactComponent as NotAllowed } from './icons/not-allowed.svg';
import './Forbidden.scss';

const Forbidden = () => (
    <div className='Forbidden'>
        <div className='Forbidden__Content'>
            <NotAllowed />
            <h1>Sorry, you do not have permission to access this page</h1>
        </div>
    </div>
);

Forbidden.propTypes = {};

Forbidden.defaultProps = {};

export default Forbidden;
