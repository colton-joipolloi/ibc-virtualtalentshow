import React from 'react';
import PropTypes from 'prop-types';
import './Loading.scss';

const Loading = () => (
    <div className='Loading'>
        <div className='lds-facebook'>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
);

Loading.propTypes = {};

Loading.defaultProps = {};

export default Loading;
