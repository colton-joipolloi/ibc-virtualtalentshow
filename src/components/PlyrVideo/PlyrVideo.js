import React, { useRef, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Plyr from 'plyr';
import 'plyr/dist/plyr.css';
import './PlyrVideo.scss';

const PlyrVideo = ({ options, src, play }) => {
    const [player, setPlayer] = useState(null);
    const videoPlyr = useRef(null);

    useEffect(() => {
        const newPlayer = new Plyr(videoPlyr.current, options);
        newPlayer.clickToPlay = true;

        // newPlayer.on('click', (event) => {
        //     newPlayer.togglePlay();
        // });

        setPlayer(newPlayer);

        return function cleanup() {
            newPlayer.destroy();
            setPlayer(null);
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (player && play && !player.playing) {
            player.play().catch((err) => {});
        }
        if (player && !play && player.playing) {
            player.stop();
        }
    }, [play, player]);

    return (
        <div className='PlyrVideo'>
            <video className='js-plyr plyr' ref={videoPlyr} playsInline>
                <source src={src} />
            </video>
        </div>
    );
};

PlyrVideo.propTypes = {
    src: PropTypes.string,
    options: PropTypes.object,
    play: PropTypes.bool.isRequired,
};

PlyrVideo.defaultProps = {
    play: false,
};

export default PlyrVideo;
