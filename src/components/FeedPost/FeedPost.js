import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { InView } from 'react-intersection-observer';
import PlyrVideo from '../PlyrVideo/PlyrVideo';
import { FeedPostProps } from '../../customProps';
import Vote from '../Vote/Vote';
import { selectVideoPlaying } from '../../store/feed/selectors';
import { setVideo } from '../../store/feed/actions';
import './FeedPost.scss';
import { PropTypes } from 'prop-types';
import { useCookies } from 'react-cookie';
import VideoHeader from '../VideoHeader/VideoHeader';

const FeedPost = ({ post, first }) => {
    const [cookies] = useCookies(['instructions']);
    const THRESHOLD = [0.5];
    const reduxDispatch = useDispatch();
    const videoPlaying = useSelector(selectVideoPlaying);

    const isInView = useCallback(
        (inView, entry) => {
            if (inView) {
                reduxDispatch(setVideo(post.url));
            }
        },
        [post.url, reduxDispatch]
    );

    let play = !videoPlaying && first ? true : videoPlaying === post.url;

    if (!cookies.instructions) {
        play = false;
    }

    return (
        <InView
            rootMargin={'60px 0px 0px 0px'}
            threshold={THRESHOLD}
            onChange={isInView}
        >
            {({ inView, ref, entry }) => (
                <div ref={ref} className='FeedPost'>
                    <div className='FeedPost__Header'>
                        <VideoHeader
                            username={post.data?.owner}
                            track={post.data?.trackName}
                            artist={post.data?.artist}
                        />
                    </div>
                    <PlyrVideo
                        play={play}
                        src={post.url}
                        options={{
                            settings: [],
                            controls: ['play-large'],
                        }}
                    ></PlyrVideo>
                    {inView && <Vote post={post}></Vote>}
                </div>
            )}
        </InView>
    );
};

FeedPost.propTypes = {
    post: FeedPostProps.isRequired,
    first: PropTypes.bool.isRequired,
};

FeedPost.defaultProps = {
    first: false,
};

export default FeedPost;
