import React, { useRef, useEffect, useState, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import Plyr from 'plyr';
import { setVideoStart, setVideoEnd } from '../../store/music/actions';
import { withStyles } from '@material-ui/core/styles';
import Slider from '@material-ui/core/Slider';
import { setVideo } from '../../store/feed/actions';
import { ReactComponent as EditorPlay } from './icons/editor-play.svg';
import { ReactComponent as EditorPause } from './icons/editor-pause.svg';
import { ReactComponent as ArrowLeft } from './icons/arrow-left.svg';
import { ReactComponent as ArrowRight } from './icons/arrow-right.svg';
import 'plyr/dist/plyr.css';
import './PlyrEditor.scss';
import { selectMusic } from '../../store/music/selectors';
import VideoHeader from '../VideoHeader/VideoHeader';
import { propTypes } from 'react-bootstrap/esm/Image';

const AirbnbSlider = withStyles({
    root: {
        color: 'transparent',
        height: 64,
        padding: '0',
    },
    thumb: {
        height: 24,
        width: 24,
        backgroundColor: '#00F3FF',
        border: '2px solid #070707',
        marginTop: 20,
        marginLeft: -10,
        '&[data-index="1"]': {
            marginLeft: -15,
        },
    },
    active: {},
    track: {
        height: 64,
        border: '5px solid #00F3FF',
        borderRadius: 11,
    },
    rail: {
        color: 'transparent',
        opacity: 1,
        height: 64,
    },
})(Slider);

function AirbnbThumbComponent(props) {
    return (
        <span {...props}>
            {props['data-index'] === 0 ? <ArrowLeft /> : <ArrowRight />}
        </span>
    );
}

const PlyrEditor = ({ src, duration, start, end, selectedTrack, username }) => {
    const videoLength = 45;
    const reduxDispatch = useDispatch();
    const { videoStart, videoEnd } = useSelector(selectMusic);
    const [value, setValue] = useState([0, videoLength]);
    const [player, setPlayer] = useState(null);
    const [playing, setPlaying] = useState(false);
    const videoPlyr = useRef(null);

    useEffect(() => {
        const newPlayer = new Plyr(videoPlyr.current, {
            settings: [],
            controls: [],
            duration,
        });
        newPlayer.clickToPlay = true;
        newPlayer.currentTime = 0;

        setPlayer(newPlayer);

        return function cleanup() {
            console.log('cleanup');
            newPlayer.destroy();
            setPlayer(null);
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        let node;
        function videoPlaying() {
            if (start && player.currentTime < start) {
                player.currentTime = start;
            }
            if (end && player.currentTime > end) {
                player.currentTime = start;
            }
            reduxDispatch(setVideo(src));
        }

        function videoCheckTime() {
            if (end && player.currentTime >= end) {
                player.stop();
                if (start) {
                    player.currentTime = start;
                }
            }
        }

        if (player && videoPlyr.current) {
            node = videoPlyr.current;
            node.addEventListener('playing', videoPlaying);
            node.addEventListener('timeupdate', videoCheckTime);
        }
        return () => {
            if (node) {
                node.removeEventListener('playing', videoPlaying);
                node.removeEventListener('timeupdate', videoCheckTime);
            }
        };
    }, [end, player, reduxDispatch, src, start]);

    const cropVideo = useCallback(
        (event, value) => {
            const { index } = event.target.dataset;
            const scrubTo = value[index];
            if (player) {
                player.pause();
                player.currentTime = scrubTo;

                if (
                    parseInt(index) === 0 &&
                    scrubTo + videoLength <= duration
                ) {
                    // Start marker
                    setValue([scrubTo, scrubTo + videoLength]);
                    reduxDispatch(setVideoStart(scrubTo));
                }

                if (parseInt(index) === 1 && scrubTo - videoLength >= 0) {
                    // End marker
                    setValue([scrubTo - videoLength, scrubTo]);
                    reduxDispatch(setVideoEnd(scrubTo));
                }
            }
        },
        [duration, player, reduxDispatch]
    );

    const handlePlay = useCallback(() => {
        player.togglePlay();
        setPlaying(player.playing);
    }, [player]);

    return (
        <div className='PlyrEditor'>
            <div className='PlyrEditor__Header'>
                <VideoHeader
                    username={username}
                    track={selectedTrack.track}
                    artist={selectedTrack.artist}
                />
            </div>
            <video className='js-plyr plyr' ref={videoPlyr}>
                <source src={src} />
            </video>
            {player && (
                <div className='PlyrEditor__Controls'>
                    <div className='PlyrEditor__Bar'>
                        {' '}
                        <button
                            className='PlyrEditor__Play'
                            onClick={handlePlay}
                        >
                            {playing ? <EditorPause /> : <EditorPlay />}
                        </button>
                    </div>
                    <div className='PlyrEditor__Slider'>
                        <AirbnbSlider
                            ThumbComponent={AirbnbThumbComponent}
                            getAriaLabel={(index) =>
                                index === 0 ? 'Video Start' : 'Video End'
                            }
                            min={0}
                            max={player.duration}
                            step={0.01}
                            value={value}
                            defaultValue={value}
                            onChange={cropVideo}
                        />
                    </div>
                </div>
            )}
        </div>
    );
};

PlyrEditor.propTypes = {
    src: PropTypes.string,
    duration: PropTypes.number,
    start: PropTypes.number,
    end: PropTypes.number,
    selectedTrack: PropTypes.object,
    username: PropTypes.string,
};

PlyrEditor.defaultProps = {};

export default PlyrEditor;
