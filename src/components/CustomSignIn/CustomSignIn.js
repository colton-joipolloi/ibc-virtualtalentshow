import React, { useState, useCallback } from 'react';
import { Auth } from 'aws-amplify';
import { useForm } from 'react-hook-form';
import PropTypes from 'prop-types';
import { ReactComponent as Logo } from './icons/logo.svg';
import classNames from 'classnames';

import './CustomSignIn.scss';
import LoadingButton from '../LoadingButton/LoadingButton';

const CustomSignIn = ({ authState, onStateChange, updateUser }) => {
    const { register, handleSubmit, getValues, errors } = useForm({
        mode: 'onChange',
    });
    const [loading, setLoading] = useState(false);
    const [newPasswordRequired, setNewPasswordRequired] = useState(false);
    const [error, setError] = useState(null);
    const _validAuthStates = ['signIn', 'signedOut', 'signedUp'];

    const onChangePassword = useCallback(
        (data) => {
            setLoading(true);
            async function changePassword() {
                try {
                    setError(null);
                    await Auth.completeNewPassword(
                        newPasswordRequired,
                        data.password
                    );
                    setLoading(false);
                    onStateChange('signedIn', {});
                } catch (err) {
                    console.log(err);
                    setLoading(false);
                }
            }
            changePassword();
        },
        [newPasswordRequired, onStateChange]
    );

    const onSubmit = useCallback(
        (data) => {
            setLoading(true);
            async function signIn() {
                try {
                    setError(null);
                    var cognitoUser = await Auth.signIn(
                        data.username,
                        data.password
                    );
                    if (cognitoUser.challengeName === 'NEW_PASSWORD_REQUIRED') {
                        setNewPasswordRequired(cognitoUser);
                    } else {
                        onStateChange('signedIn', {});
                    }

                    setLoading(false);
                } catch (err) {
                    console.log(err);
                    if (err.code === 'UserNotConfirmedException') {
                        updateUser(data);
                        await Auth.resendSignUp(data.username);
                        onStateChange('confirmSignUp', {});
                    } else if (err.code === 'NotAuthorizedException') {
                        // The error happens when the incorrect password is provided
                        setError('Login failed.');
                    } else if (err.code === 'UserNotFoundException') {
                        // The error happens when the supplied username/email does not exist in the Cognito user pool
                        setError(err.message);
                    } else {
                        setError(err.message);
                    }
                    setLoading(false);
                }
            }
            signIn();
        },
        [onStateChange, updateUser]
    );

    const getClasses = (field) => {
        const hasError = errors[field];
        const hasValue = getValues(field);
        const classes = classNames('form-input', {
            error: hasError,
            success: hasValue && !hasError,
        });
        return classes;
    };

    if (authState === 'signedIn') {
        return null;
    }

    return (
        <div className='CustomSignIn'>
            <div className='CustomSignIn__Container'>
                <div className='CustomSignIn__Intro'>
                    <Logo />
                    <p>
                        Singzta is a new singing talent show. Sign up to show
                        your vocal talent or sign up to judge on who you think
                        should win the chance of a major label record deal and
                        promotion on MTV!
                    </p>

                    <p>
                        This platform is being used for test purposes only, with no prize functions being active for the trial. 
                        Please participate if you are happy to only test the platform for research purposes.
                    </p>
                </div>
                <div className='CustomSignIn__Form'>
                    {!newPasswordRequired &&
                        _validAuthStates.includes(authState) && (
                            <form onSubmit={handleSubmit(onSubmit)}>
                                {error && (
                                    <div className='CustomSignIn__Error'>
                                        {error}
                                    </div>
                                )}
                                <div className='form-field'>
                                    <input
                                        className={getClasses('username')}
                                        id='username'
                                        key='username'
                                        name='username'
                                        type='text'
                                        placeholder='Username'
                                        ref={register({ required: true })}
                                    />
                                </div>
                                <div className='form-field'>
                                    <input
                                        className={getClasses('password')}
                                        id='password'
                                        key='password'
                                        name='password'
                                        type='password'
                                        placeholder='• • • • • • • •'
                                        ref={register({ required: true })}
                                    />
                                </div>
                                <a href='./'>Forgot you password?</a>

                                <LoadingButton
                                    classes='btn btn-solid-blue'
                                    title='Sign In'
                                    type='submit'
                                    loading={loading}
                                ></LoadingButton>
                            </form>
                        )}
                    {newPasswordRequired && (
                        <form onSubmit={handleSubmit(onChangePassword)}>
                            {error && (
                                <div className='CustomSignIn__Error'>
                                    {error}
                                </div>
                            )}

                            <p>New password required</p>
                            <input
                                className={getClasses('password')}
                                id='password'
                                key='password'
                                name='password'
                                type='password'
                                placeholder='• • • • • • • •'
                                ref={register({ required: true })}
                            />

                            <LoadingButton
                                classes='btn btn-solid-blue'
                                title='Confirm'
                                type='submit'
                                loading={loading}
                            ></LoadingButton>
                        </form>
                    )}
                </div>

                <div className='CustomSignIn__Account'>
                    <p>Not got an account?</p>
                    <a href='./'>Create an account</a>
                </div>
                <div className='CustomSignIn__Terms'>
                    <p>
                        View our <a href='./'>Terms & Conditions</a> and{' '}
                        <a href='./'>Privacy Policy</a>.
                    </p>
                </div>
            </div>
        </div>
    );
};

CustomSignIn.propTypes = {
    authState: PropTypes.string,
    onStateChange: PropTypes.func,
    updateUsername: PropTypes.func,
};

CustomSignIn.defaultProps = {};

export default CustomSignIn;
