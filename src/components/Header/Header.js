import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { ReactComponent as Home } from './icons/home.svg';
import { ReactComponent as Account } from './icons/account.svg';
import { ReactComponent as Back } from './icons/back.svg';
import './Header.scss';
import { useSelector } from 'react-redux';
import { selectIsLive, selectLiveDate } from '../../store/state/selectors';
import { format } from 'date-fns';

const Header = ({ back }) => {
    const isLive = useSelector(selectIsLive);
    const liveDate = useSelector(selectLiveDate);
    const formatDate = format(new Date(liveDate), 'do MMM');
    const formatTime = format(new Date(liveDate), 'kk:mm');

    return (
        <div className='Header'>
            <div className='Header__Container'>
                {back ? (
                    <Link to='/select'>
                        <Back />
                    </Link>
                ) : (
                    <>
                        <Link to='/'>
                            <Home />
                        </Link>
                        {isLive && (
                            <div className='Header__Live'>SHOW NOW LIVE</div>
                        )}
                        {!isLive && liveDate && (
                            <div className='Header__Live'>
                                NEXT LIVE SHOW
                                <div className='Header__Time'>
                                    {formatDate} at {formatTime} GMT
                                </div>
                            </div>
                        )}

                         <Link to='/signout'>
                            <Account />
                        </Link>
                    </>
                )}
            </div>
        </div>
    );
};

Header.propTypes = {
    back: PropTypes.bool.isRequired,
};

Header.defaultProps = {
    back: false,
};

export default Header;
