import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './PlayBtn.scss';

const PlayBtn = ({ playing, loading }) => {
    const classes = classNames('PlayBtn', { loading });
    return (
        <div className={classes}>
            <svg
                width='40'
                height='40'
                viewBox='0 0 40 40'
                xmlns='http://www.w3.org/2000/svg'
            >
                <g fill='none'>
                    <circle
                        className='PlayBtn__Loader'
                        stroke='#FFF'
                        cx='20'
                        cy='20'
                        r='19.5'
                    />
                    {!playing && !loading && (
                        <path fill='#FFF' d='M14 29V11l16 9z' />
                    )}
                    {playing && !loading && (
                        <g fill='#FFF'>
                            <path d='M14 12h4v17h-4zM22 12h4v17h-4z' />
                        </g>
                    )}
                </g>
            </svg>
        </div>
    );
};

PlayBtn.propTypes = {
    playing: PropTypes.bool.isRequired,
    loading: PropTypes.bool.isRequired,
};

PlayBtn.defaultProps = {
    playing: false,
    loading: false,
};

export default PlayBtn;
