import React, { useState, useCallback, useEffect } from 'react';
import classNames from 'classnames';
import { ReactComponent as Face } from './icons/face.svg';
import { ReactComponent as Smile } from './icons/happy-face.svg';
import { ReactComponent as Heart } from './icons/heart.svg';
import { ReactComponent as Fire } from './icons/fire.svg';
import PropTypes from 'prop-types';
import { API, graphqlOperation } from 'aws-amplify';
import * as mutations from '../../graphql/mutations';
import { useDispatch } from 'react-redux';
import { setVote } from '../../store/feed/actions';

import './Vote.scss';

const iconNames = ['Fire', 'Heart', 'Smile', 'Face'];

const Vote = ({ post }) => {
    const reduxDispatch = useDispatch();
    const [icons, setIcons] = useState([]);
    const [selectedVote, setSelectedVote] = useState(null);
    const vote = post?.data?.votes?.items[0] || {};

    useEffect(() => {
        if (vote && vote.type) {
            setSelectedVote(vote.type);
        }
    }, [vote]);

    const saveVote = useCallback(
        (icon) => {
            setSelectedVote(icon);
            async function addVote() {
                const create = await API.graphql(
                    graphqlOperation(mutations.createVote, {
                        input: {
                            type: icon,
                            postID: post.key,
                        },
                    })
                );
                reduxDispatch(setVote(create.data.createVote));
            }

            async function updateVote() {
                const update = await API.graphql(
                    graphqlOperation(mutations.updateVote, {
                        input: { id: vote.id, type: icon, postID: post.key },
                    })
                );
                reduxDispatch(setVote(update.data.updateVote));
            }

            if (post && vote && vote.id) {
                // Update vote
                updateVote();
            } else if (!vote.id) {
                // Create vote
                addVote();
            }
        },
        [post, reduxDispatch, vote]
    );

    const handleClick = useCallback(
        (icon) => {
            const iconsUpdate = [...icons, icon];
            setIcons(iconsUpdate);
            if (post) {
                saveVote(icon);
            }
        },
        [icons, post, saveVote]
    );

    const getButton = (icon) => {
        const floaters = icons.filter((i) => i === icon);

        switch (icon) {
            case 'Face':
                return (
                    <>
                        <Face />
                        <div className='Floaters'>
                            {floaters.map((f, i) => (
                                <Face key={i} />
                            ))}
                        </div>
                    </>
                );
            case 'Smile':
                return (
                    <>
                        <Smile />
                        <div className='Floaters'>
                            {floaters.map((f, i) => (
                                <Smile key={i} />
                            ))}
                        </div>
                    </>
                );
            case 'Heart':
                return (
                    <>
                        <Heart />
                        <div className='Floaters'>
                            {floaters.map((f, i) => (
                                <Heart key={i} />
                            ))}
                        </div>
                    </>
                );
            case 'Fire':
                return (
                    <>
                        <Fire />
                        <div className='Floaters'>
                            {floaters.map((f, i) => (
                                <Fire key={i} />
                            ))}
                        </div>
                    </>
                );
            default:
                return null;
        }
    };

    const getSelected = (icon) => {
        const classes = classNames('Vote__Icon', {
            selected: selectedVote && selectedVote === icon,
        });
        return classes;
    };

    return (
        <div className='Vote'>
            {iconNames.map((icon, i) => (
                <button
                    key={i}
                    className={getSelected(icon)}
                    onClick={() => handleClick(icon)}
                >
                    {getButton(icon)}
                </button>
            ))}
        </div>
    );
};

Vote.propTypes = {
    videoId: PropTypes.string,
    post: PropTypes.object,
};

Vote.defaultProps = {};

export default Vote;
