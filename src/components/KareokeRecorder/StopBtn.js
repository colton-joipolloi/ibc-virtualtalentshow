import React from 'react';
import styled from 'styled-components';

const Button = styled.button`
    background: ${(props) => props.backgroundColor};
    color: ${(props) => props.color};
    border-radius: 4px;
    width: 26px;
    height: 26px;
    background: #ffffff;
    outline: none;
    border: none;
    cursor: pointer;
    margin: 20px;
    :hover {
        background: #fb6d42;
    }
`;

const Border = styled.div`
    z-index: 1000;
    background: #ff4365;
    border: 1px solid white;
    height: 78px;
    width: 78px;
    margin-bottom: 20px;
    border-radius: 50%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`;

Button.defaultProps = {
    color: 'black',
    backgroundColor: 'white',
};

export default (props) => (
    <Border>
        <Button {...props} />
    </Border>
);
