import React, { useEffect, useRef } from 'react';
import './MicVisualizer.scss';

const MicVisualizer = () => {

    const svgRef = useRef(null);
    useEffect(() => {

        if (svgRef.current) {
            var paths = svgRef.current.getElementsByTagName("path");
            var visualizer = document.getElementById('visualizer');
            var mask = visualizer.getElementById('mask');
        }

        var path;

        if (paths && visualizer && mask) {
            var soundAllowed = function (stream) {
                // Audio stops listening in FF without // window.persistAudioStream = stream;
                // https://bugzilla.mozilla.org/show_bug.cgi?id=965483
                // https://support.mozilla.org/en-US/questions/984179
                window.persistAudioStream = stream;

                var audioContent = new AudioContext();
                var audioStream = audioContent.createMediaStreamSource(stream);
                var analyser = audioContent.createAnalyser();
                audioStream.connect(analyser);
                analyser.fftSize = 1024;

                var frequencyArray = new Uint8Array(analyser.frequencyBinCount);
                visualizer.setAttribute('viewBox', '0 0 150 150');

                // Through the frequency Array has a length longer than 255, there seems to be no
                // significant data after this point. Not worth visualizing.
                for (var i = 0; i < 255; i++) {
                    path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
                    path.setAttribute('stroke-dasharray', '4,1');
                    mask.appendChild(path);
                }

                var doDraw = function () {
                    requestAnimationFrame(doDraw);
                    analyser.getByteFrequencyData(frequencyArray);
                    var adjustedLength;
                    for (var i = 0; i < 255; i++) {
                        if (paths[i]) {
                            adjustedLength = Math.floor(frequencyArray[i]) - (Math.floor(frequencyArray[i]) % 5);
                            paths[i].setAttribute('d', 'M ' + (i) + ',150 l 0,-' + adjustedLength);
                        } else {
                            break;
                        };
                    }
                };

                doDraw();
            };
        }

        var soundNotAllowed = function (error) {
            console.log(error);
        };

        /*
        window.navigator = window.navigator || {};
        /*
        navigator.getUserMedia = navigator.getUserMedia       ||
                                navigator.webkitGetUserMedia ||
                                navigator.mozGetUserMedia    ||
                                null;*/
        navigator.getUserMedia({ audio: true }, soundAllowed, soundNotAllowed);

    }, []);
    return (
        <div className='Mic__Container'>
            <svg ref={svgRef} className='visualizer' preserveAspectRatio="none" id="visualizer" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <defs>
                    <mask id="mask">
                        <g id="maskGroup"></g>
                    </mask>
                    <linearGradient id="gradient" x1="0%" y1="0%" x2="0%" y2="100%">
                        <stop offset="0%" style={{ stopColor: '#ffffff', stopOpacity: 0.8 }} />
                        <stop offset="20%" style={{ stopColor: '#ffffff', stopOpacity: 0.7 }} />
                        <stop offset="90%" style={{ stopColor: '#00ff7f', stopOpacity: 0.6 }} />
                        <stop offset="100%" style={{ stopColor: '#050d61', stopOpacity: 0.5 }} />
                    </linearGradient>
                </defs>
                <rect x="0" y="0" width="100%" height="100%" fill="url(#gradient)" mask="url(#mask)"></rect>
            </svg>
        </div>
    );
};

export default MicVisualizer;
