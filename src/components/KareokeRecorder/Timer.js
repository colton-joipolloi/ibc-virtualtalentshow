import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Text = styled.div`
    position: absolute;
    top: 58px;
    left: 50%;
    transform: translateX(-50%);
    font-family: Gill Sans;
    font-size: 16px;
    height: 30px;
    border-radius: 15px;
    color: #ff4365;
    text-transform: uppercase;
    background-color: #050505;
    border: 1px solid #fff;
    font-weight: 600;
    padding: 0 15px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`;

class Timer extends Component {
    static propTypes = {
        timeLimit: PropTypes.number,
        defaultText: PropTypes.string,
    };

    constructor(props) {
        super(props);

        const nextSeconds = props.timeLimit ? props.timeLimit / 1000 : 0;

        this.state = this.getState(nextSeconds);
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    componentDidMount() {
        const { timeLimit } = this.props;
        this.timer = setInterval(() => {
            const { seconds } = this.state;
            const nextSeconds = timeLimit ? seconds - 1 : seconds + 1;

            const nextState = this.getState(nextSeconds);
            this.setState(nextState);
        }, 1000);
    }

    pad(unit) {
        var str = '' + unit;
        var pad = '00';
        return pad.substring(0, pad.length - str.length) + str;
    }

    getState(seconds) {
        const minutes = Math.floor(seconds / 60);

        const humanTime =
            minutes !== 0
                ? `${minutes}:${this.pad(seconds - minutes * 60)}`
                : `${seconds - minutes * 60}s`;

        return {
            seconds: seconds,
            human: humanTime,
        };
    }

    render() {
        const defaultText = this.props.defaultText || '0:00';
        return (
            <Text {...this.props}>Rec {this.state.human || defaultText}</Text>
        );
    }
}

export default Timer;
