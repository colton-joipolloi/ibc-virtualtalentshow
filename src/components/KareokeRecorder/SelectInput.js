import React from 'react'

export default class SelectInput extends React.Component {
    constructor(props) {
        super(props);

        this.state = { videoInputs: [], audioInputs: [], audioOutputs: [] };
        this.mediaRecorderOpts = this.props.mediaRecorderOpts;

        const AudioContext = window.AudioContext || window.webkitAudioContext;
        this.audioCtx = new AudioContext();
    }

    // Attach audio output device to video element using device/sink ID.
    attachSinkId(sinkId) {
        const element = document.querySelector('video');

        if (typeof element.sinkId !== 'undefined') {
            element.setSinkId(sinkId)
            .then(() => {
                console.log(`Success, audio output device attached: ${sinkId}`);
            })
            .catch(error => {
                let errorMessage = error;
                if (error.name === 'SecurityError') {
                errorMessage = `You need to use HTTPS for selecting audio output device: ${error}`;
                }
                console.error(errorMessage);
                // Jump back to first output device in the list as it's the default.
                // audioOutputSelect.selectedIndex = 0;
            });
        } else {
            console.warn('Browser does not support output device selection.');
        }
    }

    componentDidMount() {
        this.inputDevices();
    }

    inputDevices () {
        let audioInputs = [];
        let videoInputs = [];
        let audioOutputs = [];

        if (!navigator.mediaDevices || !navigator.mediaDevices.enumerateDevices) {
            console.log("enumerateDevices() not supported.");
            return;
        }
        
        // List cameras and microphones.
        navigator.mediaDevices.enumerateDevices()
        .then(function(devices) {
            console.log(devices);
            devices.forEach(function(device) {
                if (device.kind == 'audioinput'){
                    audioInputs.push({ label: device.label, id: device.deviceId });
                }
                if (device.kind == 'videoinput'){
                    videoInputs.push({ label: device.label, id: device.deviceId });
                }
                if (device.kind == 'audiooutput'){
                    audioOutputs.push({ label: device.label, id: device.deviceId });
                }
            });
        })
        .catch(function(err) {
            console.log(err.name + ": " + err.message);
        });

        this.setState({ audioInputs: audioInputs, videoInputs: videoInputs, audioOutputs, audioOutputs });
    }

    handleOutputChange(event, inputType){
        console.log(inputType, event);
        
        this.attachSinkId(event.target.value);
    }


    handleInputChange(event, inputType){
        console.log(inputType, event);

        if (inputType == 'audioInput'){
            const constraints = {
                audio: { deviceId: event.target.value }
            };

            navigator.mediaDevices.getUserMedia(constraints).catch(error =>
                {
                    console.log('navigator.MediaDevices.getUserMedia error: ', error.message, error.name);
                }
            );
        }

        if (inputType == 'videoInput'){
            const constraints = {
                video: { deviceId: event.target.value }
            };

            navigator.mediaDevices.getUserMedia(constraints).catch(error =>
                {
                    console.log('navigator.MediaDevices.getUserMedia error: ', error.message, error.name);
                }
            );
        }
    }

    render() {

        return (
            <div className='Record__Devices'>
                <select onChange={event => this.handleInputChange(event, 'audioInput')}>
                    {this.state.audioInputs.sort().map((input, index) => <option key={input.id} value={input.id}>{input.label}</option>)}
                </select>

                <select onChange={event => this.handleInputChange(event, 'videoInput')}>
                    {this.state.videoInputs.sort().map((input, index) => <option key={input.id} value={input.id}>{input.label}</option>)}
                </select>

                <select onChange={event => this.handleOutputChange(event, 'audioOutput')}>
                    {this.state.audioOutputs.sort().map((input, index) => <option key={input.id} value={input.id}>{input.label}</option>)}
                </select>
            </div>
        );
    }
}
