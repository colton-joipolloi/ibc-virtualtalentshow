import React from 'react';
import styled from 'styled-components';

const Button = styled.button`
    background: ${(props) => props.backgroundColor};
    color: ${(props) => props.color};
    border-radius: 50%;
    width: 78px;
    height: 78px;
    background: #ff4365;
    outline: none;
    border: 2px solid white;
    cursor: pointer;
    :hover {
        background: #fb6d42;
    }
`;

const RecWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    padding-bottom: 20px;
    z-index: 1000;
`;

const ButtonBorder = styled.div`
    border: 2px solid white;
    height: 106px;
    width: 106px;
    border-radius: 50%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`;

Button.defaultProps = {
    color: 'black',
    backgroundColor: 'white',
};

export default (props) => (
    <RecWrapper>
        <ButtonBorder>
            <Button {...props} />
        </ButtonBorder>
    </RecWrapper>
);
