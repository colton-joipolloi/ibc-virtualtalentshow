import React from 'react';
import styled from 'styled-components';
import Button from 'react-video-recorder/lib/defaults/button';
import RecordButton from './RecordBtn';
import StopButton from './StopBtn';
import Timer from './Timer';
import Countdown from './Countdown';

const ActionsWrapper = styled.div`
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    padding-top: 20px;
    padding-bottom: 20px;
`;
const Actions = ({
    isVideoInputSupported,
    isInlineRecordingSupported,
    thereWasAnError,
    isRecording,
    isCameraOn,
    streamIsReady,
    isConnecting,
    isRunningCountdown,
    isReplayingVideo,
    countdownTime,
    timeLimit,
    showReplayControls,
    replayVideoAutoplayAndLoopOff,
    useVideoInput,

    onTurnOnCamera,
    onTurnOffCamera,
    onOpenVideoInput,
    onStartRecording,
    onStopRecording,
    onPauseRecording,
    onResumeRecording,
    onStopReplaying,
    onConfirm,
}) => {
    const renderContent = () => {
        const shouldUseVideoInput =
            !isInlineRecordingSupported && isVideoInputSupported;

        if (
            (!isInlineRecordingSupported && !isVideoInputSupported) ||
            thereWasAnError ||
            isConnecting ||
            isRunningCountdown
        ) {
            return null;
        }

        if (isRecording) {
            return (
                <StopButton
                    onClick={onStopRecording}
                    data-qa='stop-recording'
                />
            );
        }

        if (isCameraOn && streamIsReady) {
            return (
                <RecordButton
                    onClick={onStartRecording}
                    data-qa='start-recording'
                />
            );
        }

        if (useVideoInput) {
            return (
                <Button onClick={onOpenVideoInput} data-qa='open-input'>
                    Upload a video
                </Button>
            );
        }

        return shouldUseVideoInput ? (
            <Button onClick={onOpenVideoInput} data-qa='open-input'>
                Record a video
            </Button>
        ) : (
            <Button onClick={onTurnOnCamera} data-qa='turn-on-camera'>
                Turn camera on
            </Button>
        );
    };

    return (
        <div>
            {isRecording && <Timer timeLimit={timeLimit} />}
            {isRunningCountdown && <Countdown countdownTime={countdownTime} />}
            {isReplayingVideo ? (
                <Button
                    onClick={onStopReplaying}
                    data-qa='start-replaying'
                    style={{ bottom: '30px' }}
                >
                    {' '}
                    Record again{' '}
                </Button>
            ) : (
                <ActionsWrapper>{renderContent()}</ActionsWrapper>
            )}
        </div>
    );
};

export default Actions;
