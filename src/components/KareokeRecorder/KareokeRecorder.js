import VideoRecorder from 'react-video-recorder';
import { captureThumb } from 'react-video-recorder/lib/get-video-info';
import { ReactVideoRecorderDataAvailableTimeoutError } from 'react-video-recorder/lib/custom-errors';

export default class KareokeRecorder extends VideoRecorder {
    constructor(props) {
        super(props);

        this.state = { videoInputs: [], audioInputs: [] };

        this.mediaRecorderOpts = this.props.mediaRecorderOpts;

        var { isFlipped } = this.props;

        this.isFlipped = isFlipped;

        console.log(isFlipped);

        const AudioContext = window.AudioContext || window.webkitAudioContext;
        this.audioCtx = new AudioContext();
    }

    componentDidMount() {
        super.componentDidMount();
        var { selectedAudio } = this.props;

        this.audio = new Audio(selectedAudio.url);
        this.audio.crossOrigin = 'anonymous'; // important

        this.inputDevices();
    }

    inputDevices() {
        let audioInputs = [];
        let videoInputs = [];

        if (!navigator.mediaDevices || !navigator.mediaDevices.enumerateDevices) {
            console.log("enumerateDevices() not supported.");
            return;
        }

        // List cameras and microphones.
        navigator.mediaDevices.enumerateDevices()
            .then(function (devices) {
                devices.forEach(function (device) {
                    if (device.kind == 'audioinput') {
                        audioInputs.push(device.label + " id = " + device.deviceId);
                    }
                    if (device.kind == 'videoinput') {
                        videoInputs.push(device.label + " id = " + device.deviceId);
                    }
                });
            })
            .catch(function (err) {
                console.log(err.name + ": " + err.message);
            });

        this.setState({ audioInputs: audioInputs, videoInputs: videoInputs });
    }

    async mixAudio(microphone, backingTrack, echo = false) {

        const audioContext = this.audioCtx;

        let mic = audioContext.createMediaStreamSource(microphone);
        let track = audioContext.createMediaStreamSource(backingTrack);
        let dest = audioContext.createMediaStreamDestination();

        // merge the mic & music
        if (echo == false) {

            mic.connect(dest);
            track.connect(dest);
        }
        // play the mic input in real time alongside the backing track
        else {
            var merger = audioContext.createChannelMerger(2);
            var splitter = audioContext.createChannelSplitter(2);

            var channel1 = [0, 1];
            var channel2 = [1, 0];

            var gainNode = audioContext.createGain();
            track.connect(gainNode);
            gainNode.connect(splitter);
            // We should perhaps control this via user input, via a slider? 0-1
            gainNode.gain.value = 0.5;

            splitter.connect(merger, channel1[0], channel1[1]);

            mic.connect(splitter);
            splitter.connect(merger, channel2[0], channel2[1]);

            merger.connect(dest);
            merger.connect(audioContext.destination);
        }

        return dest.stream;
    }

    startRecording = () => {
        const { microphonePlayback } = this.props;

        captureThumb(this.cameraVideo).then(async (thumbnail) => {
            this.thumbnail = thumbnail;

            this.recordedBlobs = [];

            try {
                this.audio.play(); // play the backing track

                this.setState({
                    isRunningCountdown: false,
                    isRecording: true,
                });

                this.startedAt = new Date().getTime();

                var mixedAudio = await this.mixAudio(
                    this.stream,
                    this.audio.captureStream(),
                    microphonePlayback
                );

                const mergedStream = new MediaStream([
                    ...this.stream.getVideoTracks(),
                    ...mixedAudio.getAudioTracks(),
                ]);

                this.mediaRecorder = new window.MediaRecorder(
                    mergedStream,
                    this.mediaRecorderOpts
                );

                this.mediaRecorder.addEventListener('stop', () => {
                    this.audio.pause();
                    this.audio.currentTime = 0;
                    mixedAudio.getAudioTracks()[0].stop();
                    this.handleStop();
                });

                this.mediaRecorder.addEventListener('error', this.handleError);
                this.mediaRecorder.addEventListener(
                    'dataavailable',
                    this.handleDataAvailable
                );

                const {
                    timeLimit,
                    chunkSize,
                    dataAvailableTimeout,
                } = this.props;
                this.mediaRecorder.start(chunkSize); // collect 10ms of data

                if (timeLimit) {
                    this.timeLimitTimeout = setTimeout(() => {
                        this.handleStopRecording();
                    }, timeLimit);
                }

                // mediaRecorder.ondataavailable should be called every 10ms,
                // as that's what we're passing to mediaRecorder.start() above
                if (Number.isInteger(dataAvailableTimeout)) {
                    setTimeout(() => {
                        if (this.recordedBlobs.length === 0) {
                            this.handleError(
                                new ReactVideoRecorderDataAvailableTimeoutError(
                                    dataAvailableTimeout
                                )
                            );
                        }
                    }, dataAvailableTimeout);
                }
            } catch (err) {
                console.error(
                    "Couldn't create MediaRecorder",
                    err,
                    this.mediaRecorderOpts
                );
                this.handleError(err);
            }
        });
    };
}
