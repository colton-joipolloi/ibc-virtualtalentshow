import React from 'react';

const ToggleEcho = ({ status, toggleEcho, microphonePlayback }) => (
    <div className='Record__Toggle__Echo'>
        Voice echo | <span style={{ textDecoration : status != "recording" ? 'underline' : 'none' }} onClick={() => status != "recording"? toggleEcho(!microphonePlayback) : null }>{microphonePlayback ? 'Enabled' : 'Disabled'}</span>
    </div>
);

export default ToggleEcho;
