import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { InView } from 'react-intersection-observer';
import PlyrVideo from '../PlyrVideo/PlyrVideo';
import { selectVideoPlaying } from '../../store/feed/selectors';
import { setVideo } from '../../store/feed/actions';
import { useCookies } from 'react-cookie';
import { PropTypes } from 'prop-types';
import './FeedPostDemo.scss';
import Vote from '../Vote/Vote';

const FeedPostDemo = ({ src, first }) => {
    const [cookies] = useCookies(['instructions']);
    const THRESHOLD = [0.5];
    const reduxDispatch = useDispatch();
    const videoPlaying = useSelector(selectVideoPlaying);

    const isInView = useCallback(
        (inView, entry) => {
            if (inView) {
                reduxDispatch(setVideo(src));
            }
        },
        [reduxDispatch, src]
    );

    let play = !videoPlaying && first ? true : videoPlaying === src;

    if (!cookies.instructions) {
        play = false;
    }

    return (
        <InView
            rootMargin={'60px 0px 0px 0px'}
            threshold={THRESHOLD}
            onChange={isInView}
        >
            {({ inView, ref, entry }) => (
                <div ref={ref} className='FeedPostDemo'>
                    <PlyrVideo
                        play={play}
                        src={src}
                        options={{
                            settings: [],
                            controls: ['play-large'],
                        }}
                    ></PlyrVideo>
                    {inView && <Vote></Vote>}
                </div>
            )}
        </InView>
    );
};
FeedPostDemo.propTypes = {
    first: PropTypes.bool,
    src: PropTypes.string,
};

FeedPostDemo.defaultProps = {};

export default FeedPostDemo;
