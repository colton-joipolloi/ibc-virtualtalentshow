import React from 'react';
import PropTypes from 'prop-types';
import { ReactComponent as Profile } from './icons/profile.svg';
import { ReactComponent as Music } from './icons/music.svg';
import './VideoHeader.scss';

const VideoHeader = ({ username, track, artist }) => (
    <div className='VideoHeader'>
        <div className='VideoHeader__Line User'>
            <span className='VideoHeader__Icon'>
                <Profile />
            </span>
            <span className='VideoHeader__Text'>{username}</span>
        </div>
        <div className='VideoHeader__Line Track'>
            <span className='VideoHeader__Icon'>
                <Music />
            </span>
            <span className='VideoHeader__Text'>
                {track} / {artist}
            </span>
        </div>
    </div>
);

VideoHeader.propTypes = {
    username: PropTypes.string,
    track: PropTypes.string,
    artist: PropTypes.string,
};

VideoHeader.defaultProps = {};

export default VideoHeader;
