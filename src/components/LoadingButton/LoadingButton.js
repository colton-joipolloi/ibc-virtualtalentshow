import React from 'react';
import PropTypes from 'prop-types';
import './LoadingButton.scss';

const LoadingButton = ({ title, classes, onClick, loading, type }) => (
    <button className={classes} type={type} onClick={onClick}>
        {loading ? (
            <div className='lds-ring'>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        ) : (
            title
        )}
    </button>
);

LoadingButton.propTypes = {
    title: PropTypes.string,
    classes: PropTypes.string,
    type: PropTypes.string.isRequired,
    onClick: PropTypes.func,
    loading: PropTypes.bool.isRequired,
};

LoadingButton.defaultProps = {
    loading: false,
    type: 'button',
};

export default LoadingButton;
