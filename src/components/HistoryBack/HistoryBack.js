import React from 'react';
import { withRouter } from 'react-router-dom';
import './HistoryBack.scss';

const GoBack = ({ history }) => (
    <button
        className='icon-text blue-grey-900'
        onClick={() => history.goBack()}
    >
        <i className='icon ion-android-arrow-back'></i> Back
    </button>
);

export default withRouter(GoBack);
