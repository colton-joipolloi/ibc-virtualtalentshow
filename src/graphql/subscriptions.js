/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreatePost = /* GraphQL */ `
  subscription OnCreatePost($owner: String) {
    onCreatePost(owner: $owner) {
      id
      userName
      artist
      trackName
      votes {
        items {
          id
          type
          postID
          createdAt
          updatedAt
          owner
        }
        nextToken
      }
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onUpdatePost = /* GraphQL */ `
  subscription OnUpdatePost($owner: String) {
    onUpdatePost(owner: $owner) {
      id
      userName
      artist
      trackName
      votes {
        items {
          id
          type
          postID
          createdAt
          updatedAt
          owner
        }
        nextToken
      }
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onDeletePost = /* GraphQL */ `
  subscription OnDeletePost($owner: String) {
    onDeletePost(owner: $owner) {
      id
      userName
      artist
      trackName
      votes {
        items {
          id
          type
          postID
          createdAt
          updatedAt
          owner
        }
        nextToken
      }
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onCreateVote = /* GraphQL */ `
  subscription OnCreateVote($owner: String) {
    onCreateVote(owner: $owner) {
      id
      type
      postID
      post {
        id
        userName
        artist
        trackName
        votes {
          nextToken
        }
        createdAt
        updatedAt
        owner
      }
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onUpdateVote = /* GraphQL */ `
  subscription OnUpdateVote($owner: String) {
    onUpdateVote(owner: $owner) {
      id
      type
      postID
      post {
        id
        userName
        artist
        trackName
        votes {
          nextToken
        }
        createdAt
        updatedAt
        owner
      }
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onDeleteVote = /* GraphQL */ `
  subscription OnDeleteVote($owner: String) {
    onDeleteVote(owner: $owner) {
      id
      type
      postID
      post {
        id
        userName
        artist
        trackName
        votes {
          nextToken
        }
        createdAt
        updatedAt
        owner
      }
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onCreateConfig = /* GraphQL */ `
  subscription OnCreateConfig($owner: String) {
    onCreateConfig(owner: $owner) {
      id
      liveDateTime
      isLive
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onUpdateConfig = /* GraphQL */ `
  subscription OnUpdateConfig($owner: String) {
    onUpdateConfig(owner: $owner) {
      id
      liveDateTime
      isLive
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onDeleteConfig = /* GraphQL */ `
  subscription OnDeleteConfig($owner: String) {
    onDeleteConfig(owner: $owner) {
      id
      liveDateTime
      isLive
      createdAt
      updatedAt
      owner
    }
  }
`;
