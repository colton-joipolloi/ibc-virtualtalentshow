import {
    SET_AUDIO_TRACK,
    SET_RECORDING,
    GET_DURATION,
    SET_VIDEO_START,
    SET_VIDEO_END,
    RESET_RECORDING,
} from './constants';
import getBlobDuration from 'get-blob-duration';

export function setAudioTrack(payload) {
    return {
        type: SET_AUDIO_TRACK,
        payload,
    };
}

export function setVideoStart(payload) {
    return {
        type: SET_VIDEO_START,
        payload,
    };
}

export function setVideoEnd(payload) {
    return {
        type: SET_VIDEO_END,
        payload,
    };
}

export function setRecording(payload) {
    return (dispatch) => {
        dispatch({
            type: GET_DURATION,
            fetching: true,
        });
        getDuration(dispatch, payload);
    };
}

async function getDuration(dispatch, blob) {
    const duration = await getBlobDuration(blob);
    dispatch({
        type: SET_RECORDING,
        payload: {
            videoBlob: blob,
            videoUrl: window.URL.createObjectURL(blob),
            videoDuration: duration,
        },
    });
}

export function resetRecording() {
    return {
        type: RESET_RECORDING,
    };
}
