import { createSelector } from 'reselect';

const getMusic = (state) => state.music;

export const selectMusic = createSelector(getMusic, (music) => music);

const getSelectedTrack = (state) => state.music.selectedTrack;

export const selectSelectedTrack = createSelector(
    getSelectedTrack,
    (track) => track
);

const getRecording = (state) => state.music.videoBlob;

export const selectRecording = createSelector(
    getRecording,
    (videoBlob) => videoBlob
);

const getVideoUrl = (state) => state.music.videoUrl;

export const selectVideoUrl = createSelector(
    getVideoUrl,
    (videoUrl) => videoUrl
);

const getVideoDuration = (state) => state.music.videoDuration;

export const selectVideoDuration = createSelector(
    getVideoDuration,
    (videoDuration) => videoDuration
);
