import {
    SET_AUDIO_TRACK,
    SET_RECORDING,
    SET_VIDEO_START,
    SET_VIDEO_END,
    RESET_RECORDING,
} from './constants';

export const initialState = {
    videoBlob: null,
    videoUrl: null,
    videoDuration: 0,
    videoStart: 0,
    videoEnd: 0,
    selectedTrack: {
        url: null,
        name: null,
        artist: null,
        category: null,
    },
};

const musicReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_AUDIO_TRACK:
            return {
                ...state,
                selectedTrack: action.payload,
            };
        case SET_VIDEO_START:
            return {
                ...state,
                videoStart: action.payload,
            };
        case SET_VIDEO_END:
            return {
                ...state,
                videoEnd: action.payload,
            };
        case SET_RECORDING:
            return {
                ...state,
                videoBlob: action.payload.videoBlob,
                videoUrl: action.payload.videoUrl,
                videoDuration: action.payload.videoDuration,
                videoEnd: action.payload.videoDuration,
            };
        case RESET_RECORDING:
            return {
                ...state,
                videoBlob: null,
                videoUrl: null,
                videoDuration: 0,
                videoStart: 0,
                videoEnd: 0,
            };
        default:
            return state;
    }
};

export default musicReducer;
