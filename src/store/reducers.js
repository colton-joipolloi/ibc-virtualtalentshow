import { combineReducers } from 'redux';
import stateReducer from './state/reducer';
import musicReducer from './music/reducer';
import feedReducer from './feed/reducer';

export default function createReducer() {
    const rootReducer = combineReducers({
        state: stateReducer,
        music: musicReducer,
        feed: feedReducer,
    });

    return rootReducer;
}
