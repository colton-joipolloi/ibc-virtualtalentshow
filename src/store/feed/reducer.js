import { SET_VIDEO, SET_FEED_POSTS, SET_VOTE } from './constants';

export const initialState = {
    videoPlaying: null,
    posts: null,
};

const feedReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_VIDEO:
            return {
                ...state,
                videoPlaying: action.payload,
            };
        case SET_FEED_POSTS:
            return {
                ...state,
                posts: action.payload,
            };
        case SET_VOTE:
            return {
                ...state,
                posts: state.posts.map((p) => {
                    if (p.key === action.payload.postID) {
                        p.data = {
                            ...p.data,
                            votes: {
                                items: [action.payload],
                            },
                        };
                    }
                    return p;
                }),
            };
        default:
            return state;
    }
};

export default feedReducer;
