import { createSelector } from 'reselect';

const getVideoPlaying = (state) => state.feed.videoPlaying;

export const selectVideoPlaying = createSelector(
    getVideoPlaying,
    (videoPlaying) => videoPlaying
);

const getFeedPosts = (state) => state.feed.posts;

export const selectFeedPosts = createSelector(getFeedPosts, (posts) => posts);
