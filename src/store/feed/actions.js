import { SET_VIDEO, SET_FEED_POSTS, SET_VOTE } from './constants';

export function setVideo(payload) {
    return {
        type: SET_VIDEO,
        payload,
    };
}

export function setFeedPosts(payload) {
    return {
        type: SET_FEED_POSTS,
        payload,
    };
}

export function setVote(payload) {
    return {
        type: SET_VOTE,
        payload,
    };
}
