import { createSelector } from 'reselect';

const getIsLive = (state) => state.state.isLive;

export const selectIsLive = createSelector(getIsLive, (isLive) => isLive);

const getLiveDate = (state) => state.state.liveDateTime;

export const selectLiveDate = createSelector(
    getLiveDate,
    (liveDateTime) => liveDateTime
);

const getUsername = (state) => state.state.username;

export const selectUsername = createSelector(
    getUsername,
    (username) => username
);

const getUserGroups = (state) => state.state.groups;

export const selectUserGroups = createSelector(
    getUserGroups,
    (groups) => groups
);
