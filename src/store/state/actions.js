import { SET_USERNAME, SET_USER, SET_LIVE_SHOW } from './constants';

export function setLiveShow(payload) {
    return {
        type: SET_LIVE_SHOW,
        payload,
    };
}

export function setUsername(payload) {
    return {
        type: SET_USERNAME,
        payload,
    };
}

export function setUser(payload) {
    return {
        type: SET_USER,
        payload,
    };
}
