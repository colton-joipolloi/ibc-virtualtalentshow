import { SET_USERNAME, SET_USER, SET_LIVE_SHOW } from './constants';

export const initialState = {
    username: null,
    groups: [],
    isLive: false,
    liveDateTime: null,
};

const stateReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_LIVE_SHOW:
            return {
                ...state,
                isLive: action.payload.isLive,
                liveDateTime: action.payload.liveDateTime,
            };
        case SET_USERNAME:
            return {
                ...state,
                username: action.payload,
            };
        case SET_USER:
            return {
                ...state,
                username: action.payload.email,
                groups: action.payload['cognito:groups'],
            };
        default:
            return state;
    }
};

export default stateReducer;
