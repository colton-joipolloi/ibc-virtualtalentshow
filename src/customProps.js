import PropTypes from 'prop-types';

export const FeedPostProps = PropTypes.shape({
    key: PropTypes.string,
    url: PropTypes.string,
});
