'use strict';
var AWS = require('aws-sdk');
var http = require("https");
const { resolve } = require('path');

const R = new AWS.Rekognition(),
S3 = new AWS.S3();

// post request
// see: https://medium.com/@sabljakovich/http-post-request-from-node-js-in-aws-lambda-826d57f0680

const defaultOptions = {
    host: 'example.com',
    post: 443,
    headers: {
        'Content-Type': 'application/json'
    }
}

const post = (path, payload) => new Promise((resolve, reject) => {
    const options = { ...defaultOptions, path, method: 'POST' };
    const req = http.request(options, res => {
        let buffer = "";
        res.on('data', chunk => buffer += chunk)
        res.on('end', () => resolve(JSON.parse(buffer)))
    });
    req.on('error', e => reject(e.message));
    req.write(JSON.stringify(payload));
    req.end();
});

exports.handler = async function(event, context, callback) {

    var message = event.Records[0].Sns.Message;
    console.log('Message received from SNS:', message);

    if (!message.Status == "SUCCEEDED"){
        context.fail('Rekognition failed to analyse the video');
    }

    message = JSON.parse(message);
    console.log(message.Video);

    var sourceFilename = message.Video.S3ObjectName;
    var bucket = message.Video.S3Bucket;

    // Strip any spaces from the source filename.
    var srcKey = decodeURIComponent(sourceFilename.replace(/\+/g, " "));
    // Strip out any dots from the filename and replace with dashes, this will become our 'folder'
    // name in the destination bucket.
    var destinationFolder = sourceFilename.replace(/\./, '-');

    const rekognitionData = await R.getContentModeration({
        JobId: message.JobId
    }).promise();

    const metaData = (await (S3.getObject({
        Bucket: bucket, 
        Key: destinationFolder.replace('web-mp4', 'metadata.json') 
    }).promise())).Body.toString('utf-8');
    
    await S3.upload({
        Bucket: bucket,
        Key: destinationFolder.replace('web-mp4', 'rekognition-data.json'),
        ContentType: "application/json",
        Body: JSON.stringify(rekognitionData),
    }).promise();
    
    console.log(metaData, rekognitionData);

    var items = [{
        id : metaData.ETag,
        author: metaData.Metadata.username,
        timestamp: metaData.LastModified,
        profile_id: metaData.Metadata.username,
        videos: [{
            url : bucket + "/" + sourceFilename
        }]
    }];

    var requestData = {
        items: items
    };

    console.log('postRequest: ', requestData);

    // const submit = await post("/", data);
};