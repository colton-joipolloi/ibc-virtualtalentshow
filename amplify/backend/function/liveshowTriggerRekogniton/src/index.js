'use strict';
var AWS = require('aws-sdk');

const R = new AWS.Rekognition(),
S3 = new AWS.S3();

exports.handler = function(event, context) {
    var sourceBucket = event.Records[0].s3.bucket.name;

    // this should be the .mp4
    var sourceFilename = event.Records[0].s3.object.key;

    if (sourceBucket !== 'liveshow-transcoded-uploads-develop') {
        context.fail('Wrong video source bucket');
        return;
    }

    async function writeMetadataToSource(){
        console.log('sourceFileName: ', sourceFilename);
        console.log('sourceFileNameReplace: ', sourceFilename.replace('web.mp4', 'metadata.json'));

        const metaData = (await (S3.getObject({
            Bucket: sourceBucket, 
            Key: sourceFilename.replace('web.mp4', 'metadata.json') 
        }).promise())).Body.toString('utf-8');
    
        // copy the metadata to the file
        await S3.copyObject({
            Bucket: sourceBucket,
            Key: sourceFilename,
            CopySource: sourceFilename,
            Metadata: metaData.Metadata,
            MetadataDirective: 'REPLACE'
        }).promise();
    }

    // Strip any spaces from the source filename.
    var srcKey = decodeURIComponent(sourceFilename.replace(/\+/g, " "));
    // Strip out any dots from the filename and replace with dashes, this will become our 'folder'
    // name in the destination bucket.
    var destinationFolder = sourceFilename.replace(/\./, '-');
    destinationFolder = destinationFolder.replace('web-mp4', 'rekognition-data.json');

    console.log(
        'srcBckt: ' + sourceBucket, 
        'srcFile: ' + sourceFilename, 
        'destKey: ' + destinationFolder
    );

    async function rekognition(){
        await S3.upload({
            Bucket: sourceBucket,
            Key: destinationFolder,
            ContentType: "application/json",
            Body: JSON.stringify(
                await R.startContentModeration({
                    Video: {
                        S3Object: {
                            Bucket: sourceBucket,
                            Name: sourceFilename,
                        },
                    },
                    NotificationChannel: {
                        RoleArn: 'arn:aws:iam::431576763507:role/Rekognition',
                        SNSTopicArn: 'arn:aws:sns:eu-west-2:431576763507:LiveShowRekognition',
                    }
                }).promise()
            ),
        }).promise();
    }
    // This isn't much use!
    // writeMetadataToSource();
    rekognition();
};