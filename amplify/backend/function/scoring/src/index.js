/* Amplify Params - DO NOT EDIT
	API_VOTING_GRAPHQLAPIENDPOINTOUTPUT
	API_VOTING_GRAPHQLAPIIDOUTPUT
	ENV
	REGION
Amplify Params - DO NOT EDIT */

/* eslint-disable no-console */
require("es6-promise").polyfill();
require("isomorphic-fetch");
// eslint-disable-next-line import/no-extraneous-dependencies

const AWS = require("aws-sdk");
const AWSAppSyncClient = require("aws-appsync").default;
const { AUTH_TYPE } = require("aws-appsync");
const S3 = new AWS.S3();

const region = process.env.REGION;
AWS.config.update({
  region
});
const appsyncUrl = process.env.API_VOTING_GRAPHQLAPIENDPOINTOUTPUT;
const gql = require("graphql-tag");

// graphql client.  We define it outside of the lambda function in order for it to be reused during subsequent calls
let client;

const currentDateTime = new Date();

// assign numerical values to the vote types

function getVoteTypeValue(type) {
    let voteType = type.toLowerCase();
    let value = 0;
    switch(voteType) {
        case 'fire':
            value = 100;
        break;
        case 'heart':
            value = 75;
        break;
        case 'smile':
            value = 50;
        break;
        case 'face':
           value = 25;
        break;
        default:
            value = 0;
    }
    return value;
}

// used to parse different types of query results to return only the item.
function parseResults(operationName, data) {
  if (operationName.includes("List")) {
    return data[`l${operationName.substring(1, operationName.length)}`];
  }
  if (operationName.includes("ListPosts")) {
    return data[`g${operationName.substring(1, operationName.length)}`];
  }
  if (operationName.includes("GetPost")) {
    return data[`g${operationName.substring(1, operationName.length)}`];
  }
  return data[operationName];
}

// initializes our graphql client
function initializeClient() {
  client = new AWSAppSyncClient({
    url: appsyncUrl,
    region,
    auth: {
        type: AUTH_TYPE.AWS_IAM,
        credentials: AWS.config.credentials
    },
    disableOffline: true
  });
}

// generic mutation function.  A way to quickly reuse mutation statements
async function executeMutation(mutation, operationName, variables) {
  if (!client) {
    initializeClient();
  }

  try {
    const response = await client.mutate({
      mutation: gql(mutation),
      variables,
      fetchPolicy: "network-only"
    });
    return parseResults(operationName, response.data);
  } catch (err) {
    console.log("Error while trying to mutate data");
    throw JSON.stringify(err);
  }
}

// generic query function.  A way to quickly reuse query statements
async function executeQuery(query, operationName, variables) {
  if (!client) {
    initializeClient();
  }
  try {
    const response = await client.query({
      query: gql(query),
      variables,
      fetchPolicy: "network-only"
    });
    return parseResults(operationName, response.data);
  } catch (err) {
    console.log("Error while trying to fetch data");
    console.log(JSON.stringify(err));
    throw JSON.stringify(err);
  }
}

exports.handler = async (event) => {

    const posts = await executeQuery(
        `query ListPosts  {
            listPosts {
                items {
                    id
                    userName
                    artist
                    trackName
                    votes {
                        items { 
                            type,
                            owner
                        }
                    }
                    createdAt
                    updatedAt
                    owner
                }
            }
        }`,
        "ListPosts",
        ""
    );

    const bucket = 'transcoded-uploads153015-develop';
    // just for initial dev whilst stuff may be uploaded
    // const bucket = 'liveshow-transcoded-uploads-develop';
    
	var params = { 
        Bucket: bucket
    }

    let s3Objects;
	let files = {};
	let response = {};

    try {
        s3Objects = await S3.listObjectsV2(params).promise();
        s3Objects = s3Objects.Contents;
       
        for (const file of s3Objects) {
            const split = file.Key.split('/');
            const fileKey = split[1];
            const fileName = split[2];
            
            if (!files[fileKey]){
                files[fileKey] = {};
            }
            
            if (fileName == 'web.mp4'){
                var p = new Promise(function(resolve,reject) {
                     S3.getSignedUrl('getObject', {
                        Bucket: bucket, 
                        Key: file.Key
                    }, function(err, url) { resolve(url); });
                });
                                
                files[fileKey].url = await p;
            }
            
            if (fileName == 'rekognition-data.json'){
                const json = (await (S3.getObject({
                    Bucket: bucket, 
                    Key: file.Key
                }).promise())).Body.toString('utf-8');
                
                files[fileKey].rekognitionData = JSON.parse(json);
            } 
            
            if (fileName == 'metadata.json'){
                const json = (await (S3.getObject({
                    Bucket: bucket, 
                    Key: file.Key
                }).promise())).Body.toString('utf-8');
                
                files[fileKey].metaData = JSON.parse(json);
            }
		}
		
        var entries = [];
        
        console.log(JSON.stringify(files));
		
		for (const [key, element] of Object.entries(files)) {
            console.log('1:', element);
            console.log('2:', element.metaData);

			let post = {};
			
			post['id'] = key;
			post['author'] = element.metaData.Metadata.username;
            post['createdAt'] = element.metaData.LastModified;

            var postData = await executeQuery(
                `query GetPost($id: ID!) {
                    getPost(id: $id) {
                        id
                        userName
                        artist
                        trackName
                        votes {
                            items {
                                id
                                type
                                postID
                                createdAt
                                updatedAt
                                owner
                            }
                            nextToken
                        }
                        createdAt
                        updatedAt
                        owner
                    }
                }
                `,
                "getPost",
                {
                    id: key
                }
            );
            
            post['votes'] = postData? postData.votes.items : null;

            let totalVotes = postData? postData.votes.items.length: 0;
            let voteTypeTotal = {};
            let voteValueTotal = 0;

            if (post['votes']){
                for (const [key, element] of Object.entries(post['votes'])) {
                    if (!voteTypeTotal[element.type]){
                        voteTypeTotal[element.type] = {};
                        voteTypeTotal[element.type].count = 0;
                        voteTypeTotal[element.type].value = 0;
                    }
    
                    voteTypeTotal[element.type].count += 1;
                    voteTypeTotal[element.type].value += getVoteTypeValue(element.type);
                    voteValueTotal += getVoteTypeValue(element.type);
                }
            }

            post['totalVotes'] = totalVotes;
            post['voteTypeTotal'] = voteTypeTotal;
            post['voteValueTotal'] = voteValueTotal
            post['refactoredVoteValue'] = voteValueTotal / totalVotes;

            var currentDateTimeEpoch = currentDateTime.valueOf();
            var createdAtEpoch = new Date(post['createdAt']).valueOf();
            post['hoursSinceCreated'] = (currentDateTimeEpoch - createdAtEpoch) / 3600000;

			// element['profile_id'] = entry.metaData.Metadata.username;
			post['video'] = element.url;

			entries.push(post);
		}

        response['scores'] = entries;
        response['currentTime'] = currentDateTime;
       
    } catch (e) {
       console.log(e)
    }

    return {
        statusCode: 200,
        body: JSON.stringify( response || null )
    }
};