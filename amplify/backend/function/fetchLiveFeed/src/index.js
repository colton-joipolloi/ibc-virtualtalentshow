/* Amplify Params - DO NOT EDIT
	API_VOTING_CONFIGTABLE_ARN
	API_VOTING_CONFIGTABLE_NAME
	API_VOTING_GRAPHQLAPIIDOUTPUT
	API_VOTING_POSTTABLE_ARN
	API_VOTING_POSTTABLE_NAME
	API_VOTING_VOTETABLE_ARN
	API_VOTING_VOTETABLE_NAME
Amplify Params - DO NOT EDIT */

'use strict';
const AWS = require('aws-sdk');
const S3 = new AWS.S3();

exports.handler = async (event) => {
    
    const bucket = 'liveshow-transcoded-uploads-develop';
    
	var params = { 
        Bucket: bucket
    }

    let s3Objects;
	let files = {};
	let response = {};

    try {
        s3Objects = await S3.listObjectsV2(params).promise();
        s3Objects = s3Objects.Contents;
       
        for (const file of s3Objects) {
            const split = file.Key.split('/');
            const fileKey = split[1];
            const fileName = split[2];
            
            if (!files[fileKey]){
                files[fileKey] = {};
            }
            
            if (fileName == 'web.mp4'){
                var p = new Promise(function(resolve,reject) {
                     S3.getSignedUrl('getObject', {
                        Bucket: bucket, 
                        Key: file.Key
                    }, function(err, url) { resolve(url); });
                });
                                
                files[fileKey].url = await p;
            }
            
            if (fileName == 'rekognition-data.json'){
                const json = (await (S3.getObject({
                    Bucket: bucket, 
                    Key: file.Key
                }).promise())).Body.toString('utf-8');
                
                files[fileKey].rekognitionData = JSON.parse(json);
            } 
            
            if (fileName == 'metadata.json'){
                const json = (await (S3.getObject({
                    Bucket: bucket, 
                    Key: file.Key
                }).promise())).Body.toString('utf-8');
                
                files[fileKey].metaData = JSON.parse(json);
            }
		}
		
		// build the actual response in the BeeOn designated format
		var liveEntries = [];
		
		for (const [key, element] of Object.entries(files)) {
		    console.log(element);
		   
			let el = {};
			
			el['id'] = key;
			el['author'] = element.metaData.Metadata.username;
			el['timestamp'] = element.metaData.LastModified;
			el['message'] = element.rekognitionData;
			// el['profile_id'] = entry.metaData.Metadata.username;
			el['videos'] = [{ url : element.url }];

			liveEntries.push(el);
		}

		response['items'] = liveEntries;
       
    } catch (e) {
       console.log(e)
    }

    return {
        statusCode: 200,
        body: JSON.stringify( response || null )
    }
};