/*
  trigger elastic transcoder, we need to move the raw uploads and generate mp4s etc...
*/

'use strict';
var AWS = require('aws-sdk');
const S3 = new AWS.S3();

var ElasticTranscoder = new AWS.ElasticTranscoder({
  region: 'eu-west-1'
});

exports.handler = async function(event, context) {
  var sourceBucket = event.Records[0].s3.bucket.name;
  var sourceFilename = event.Records[0].s3.object.key;

  if (sourceBucket !== 'raw-uploads145703-develop') { // CHANGE ME
    context.fail('Wrong video source bucket');
    return;
  }

  // Define the Elastic Transcoder pipeline.
  var pipelineId = '1597322971905-zeq6xo';
  // Define the preset we want to use.
  var presetId = '1351620000001-000010';
  // Strip any spaces from the source filename.
  var srcKey = decodeURIComponent(sourceFilename.replace(/\+/g, " "));
  // Strip out any dots from the filename and replace with dashes, this will become our 'folder'
  // name in the destination bucket.
  var removeExt = sourceFilename.split('.');
  removeExt = removeExt[0];
  
  var destinationFolder = removeExt.replace(/\./, '-');

  // Extract the metadata from the video
  var fetchMetaData = function () {
    return new Promise(function (resolve, reject) {
      S3.headObject({
        Bucket: sourceBucket,
        Key: sourceFilename
      }, function(err, data){
          if (err) { reject(err) }
          else { resolve(data) }
      });
    });
  };
  
  await fetchMetaData().then( async function (data) {
    console.log(data);
    console.log(data.Metadata);

    // Transcode
    var params = {
      PipelineId: pipelineId,
      Input: {
        Key: srcKey,
        FrameRate: 'auto',
        Resolution: 'auto',
        AspectRatio: 'auto',
        Interlaced: 'auto',
        Container: 'auto'
      },
      Outputs: [
        {
          // Full length video and thumbnails.
          Key: destinationFolder + '/web.mp4',
          // ThumbnailPattern: destinationFolder + '/thumbs-{count}', // Must include {count}
          PresetId: presetId,
          Composition: [{
            TimeSpan: {
              StartTime: data.Metadata.starttime,
              Duration: data.Metadata.endtime
            }
          }] 
        }
      ],
      UserMetadata: data.Metadata
    };
    
    console.log(params);
  
    ElasticTranscoder.createJob(params, function(err, d){
      if (err) {
        console.log(err);
        context.fail();
        return;
      }
    });

    // Upload the metadata to the destination bucket
    try {
      const putResult = await S3.upload({
        Bucket: 'transcoded-uploads153015-develop',
        Key: destinationFolder + '/metadata.json',
        ContentType: "application/json",
        Body: JSON.stringify(data)
      }).promise();
      
    } catch (error) {
      console.log(error);
      return;
    }
  })
};